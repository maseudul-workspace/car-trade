package in.gadidekho.gadidekho;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;

import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.UserInfo;


/**
 * Created by Raj on 22-07-2019.
 */

public class AndroidApplication extends Application {

    UserInfo userInfo;
    AuctionVehicle[] wishLists;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setUserInfo(Context context, UserInfo userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                "CarDekho", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString("USER", new Gson().toJson(userInfo));
        } else {
            editor.putString("USER", "");
        }

        editor.commit();
    }

    public void setWishLists(Context context, AuctionVehicle[] auctionVehicles){
        this.wishLists = auctionVehicles;
        SharedPreferences sharedPref = context.getSharedPreferences(
                "CarDekho", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(auctionVehicles != null) {
            editor.putString("WISHLIST", new Gson().toJson(auctionVehicles));
        } else {
            editor.putString("WISHLIST", "");
        }

        editor.commit();
    }

    public UserInfo getUserInfo(Context context){
        UserInfo user;
        if(userInfo != null){
            user = userInfo;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    "CarDekho", Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString("USER","");
            if(userInfoJson.isEmpty()){
                user = null;
            }else{
                try {
                    user = new Gson().fromJson(userInfoJson, UserInfo.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

    public AuctionVehicle[] getWishLists(Context context){
        AuctionVehicle[] auctionVehicles;
        if(wishLists != null){
            auctionVehicles = wishLists;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    "CarDekho", Context.MODE_PRIVATE);
            String wishlistJson = sharedPref.getString("WISHLIST","");
            if(wishlistJson.isEmpty()){
                auctionVehicles = null;
            }else{
                try {
                    auctionVehicles = new Gson().fromJson(wishlistJson, AuctionVehicle[].class);
                    this.wishLists = auctionVehicles;
                }catch (Exception e){
                    auctionVehicles = null;
                }
            }
        }
        return auctionVehicles;
    }

}
