package in.gadidekho.gadidekho.repository;

import android.content.Context;
import android.util.Log;

import com.google.gson.Gson;

import java.io.File;

import in.gadidekho.gadidekho.domain.models.AuctionVehicleWrapper;
import in.gadidekho.gadidekho.domain.models.AuctionWrapper;
import in.gadidekho.gadidekho.domain.models.DepositLimitWrapper;
import in.gadidekho.gadidekho.domain.models.NotificationsWrapper;
import in.gadidekho.gadidekho.domain.models.Payments;
import in.gadidekho.gadidekho.domain.models.PaymentsWrapper;
import in.gadidekho.gadidekho.domain.models.SuccessResponse;
import in.gadidekho.gadidekho.domain.models.UpcomingAuction;
import in.gadidekho.gadidekho.domain.models.UpcomingAuctionWrapper;
import in.gadidekho.gadidekho.domain.models.UserDetails;
import in.gadidekho.gadidekho.domain.models.UserDetailsWrapper;
import in.gadidekho.gadidekho.domain.models.UserInfoWrapper;
import in.gadidekho.gadidekho.domain.models.VehicleDetailsWrapper;
import in.gadidekho.gadidekho.presentation.ui.adapters.AuctionAdapter;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = APIclient.createService(AppRepository.class);
    }

    public SuccessResponse registerUser(String name, String email, String mobile, String password, String address, String addressProofFile, String idProofFile) {
        SuccessResponse successResponse;
        RequestBody memberName = RequestBody.create(okhttp3.MultipartBody.FORM, name);
        RequestBody memberEmail = RequestBody.create(okhttp3.MultipartBody.FORM, email);
        RequestBody memberMobile = RequestBody.create(okhttp3.MultipartBody.FORM, mobile);
        RequestBody memberPassword = RequestBody.create(okhttp3.MultipartBody.FORM, password);
        RequestBody memberAddress = RequestBody.create(okhttp3.MultipartBody.FORM, address);

        File addressfile = new File(addressProofFile);

        // create RequestBody instance from file
        RequestBody requestAddressFile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        addressfile
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part addressBody =
                MultipartBody.Part.createFormData("address_proof", addressfile.getName(), requestAddressFile);

        File idFile = new File(idProofFile);

        // create RequestBody instance from file
        RequestBody requestIdFile =
                RequestBody.create(
                        MediaType.parse("multipart/form-data"),
                        idFile
                );

        // MultipartBody.Part is used to send also the actual file name
        MultipartBody.Part idBody =
                MultipartBody.Part.createFormData("id_proof", idFile.getName(), requestIdFile);

        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.registerUser(memberName, memberEmail, memberMobile, memberPassword, memberAddress, addressBody, idBody);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    successResponse = null;
                }else{
                    successResponse = gson.fromJson(responseBody, SuccessResponse.class);
                }
            } else {
                successResponse = null;
            }
        }catch (Exception e){
            successResponse = null;
        }

        return successResponse;
    }

    public UserInfoWrapper checkLogin(String phone, String password) {
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkLogin(phone, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public AuctionWrapper getAuctions(int userId, String apiToken) {
        AuctionWrapper auctionWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchAuction(userId, apiToken);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    auctionWrapper = null;
                }else{
                    auctionWrapper = gson.fromJson(responseBody, AuctionWrapper.class);
                }
            } else {
                auctionWrapper = null;
            }
        }catch (Exception e){
            auctionWrapper = null;
        }
        return auctionWrapper;
    }

    public AuctionVehicleWrapper fetchAuctionVehiclesWrapper(int userId, String apiToken, int auctionId) {
        AuctionVehicleWrapper auctionVehicleWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchAuctionVehicles(userId, apiToken, auctionId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    auctionVehicleWrapper = null;
                }else{
                    auctionVehicleWrapper = gson.fromJson(responseBody, AuctionVehicleWrapper.class);
                }
            } else {
                auctionVehicleWrapper = null;
            }
        }catch (Exception e){
            auctionVehicleWrapper = null;
        }
        return auctionVehicleWrapper;
    }

    public VehicleDetailsWrapper fetchVeicleDetails(int userId, String apiToken, int auctionId, int vehicleId) {
        VehicleDetailsWrapper vehicleDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchVehicleDetails(userId, apiToken, auctionId, vehicleId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    vehicleDetailsWrapper = null;
                }else{
                    vehicleDetailsWrapper = gson.fromJson(responseBody, VehicleDetailsWrapper.class);
                }
            } else {
                vehicleDetailsWrapper = null;
            }
        }catch (Exception e){
            vehicleDetailsWrapper = null;
        }
        return vehicleDetailsWrapper;
    }

    public SuccessResponse submitBid(int userId, String apiToken, int vehicleId, int bidAmount) {
        SuccessResponse successResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.submitBid(userId, apiToken, vehicleId, bidAmount);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    successResponse = null;
                }else{
                    successResponse = gson.fromJson(responseBody, SuccessResponse.class);
                }
            } else {
                successResponse = null;
            }
        }catch (Exception e){
            successResponse = null;
        }
        return successResponse;
    }

    public NotificationsWrapper getNotifications(int userId, String apiToken) {
        NotificationsWrapper notificationsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchNotifications(userId, apiToken);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    notificationsWrapper = null;
                }else{
                    notificationsWrapper = gson.fromJson(responseBody, NotificationsWrapper.class);
                }
            } else {
                notificationsWrapper = null;
            }
        }catch (Exception e){
            notificationsWrapper = null;
        }
        return notificationsWrapper;
    }

    public AuctionVehicleWrapper fetchMyBids(int userId, String apiToken) {
        AuctionVehicleWrapper auctionVehicleWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchMyBids(userId, apiToken);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    auctionVehicleWrapper = null;
                }else{
                    auctionVehicleWrapper = gson.fromJson(responseBody, AuctionVehicleWrapper.class);
                }
            } else {
                auctionVehicleWrapper = null;
            }
        }catch (Exception e){
            auctionVehicleWrapper = null;
        }
        return auctionVehicleWrapper;
    }

    public PaymentsWrapper fetchPayments(int userId, String apiToken) {
        PaymentsWrapper paymentsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchMyPayments(userId, apiToken);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    paymentsWrapper = null;
                }else{
                    paymentsWrapper = gson.fromJson(responseBody, PaymentsWrapper.class);
                }
            } else {
                paymentsWrapper = null;
            }
        }catch (Exception e){
            paymentsWrapper = null;
        }
        return paymentsWrapper;
    }

    public SuccessResponse addToWishList(int userId, String apiToken, int vehicleId) {
        SuccessResponse successResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.addToWishList(userId, apiToken, vehicleId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    successResponse = null;
                }else{
                    successResponse = gson.fromJson(responseBody, SuccessResponse.class);
                }
            } else {
                successResponse = null;
            }
        }catch (Exception e){
            successResponse = null;
        }
        return successResponse;
    }

    public AuctionVehicleWrapper fetchWishList(int userId, String apiKey) {
        AuctionVehicleWrapper auctionVehicleWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchWishList(userId, apiKey);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    auctionVehicleWrapper = null;
                }else{
                    auctionVehicleWrapper = gson.fromJson(responseBody, AuctionVehicleWrapper.class);
                }
            } else {
                auctionVehicleWrapper = null;
            }
        }catch (Exception e){
            auctionVehicleWrapper = null;
        }
        return auctionVehicleWrapper;
    }

    public SuccessResponse removeFromWishList(int userId, String apiToken, int vehicleId) {
        SuccessResponse successResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.removeFromWishList(userId, apiToken, vehicleId);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    successResponse = null;
                }else{
                    successResponse = gson.fromJson(responseBody, SuccessResponse.class);
                }
            } else {
                successResponse = null;
            }
        }catch (Exception e){
            successResponse = null;
        }
        return successResponse;
    }

    public DepositLimitWrapper fetchDepositLimit(int userId, String apiKey) {
        DepositLimitWrapper depositLimitWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchDepositLimit(userId, apiKey);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    depositLimitWrapper = null;
                }else{
                    depositLimitWrapper = gson.fromJson(responseBody, DepositLimitWrapper.class);
                }
            } else {
                depositLimitWrapper = null;
            }
        }catch (Exception e){
            depositLimitWrapper = null;
        }
        return depositLimitWrapper;
    }

    public UserDetailsWrapper fetchUserDetails(int userId, String apiKey) {
        UserDetailsWrapper userDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchUserDetails(userId, apiKey);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userDetailsWrapper = null;
                }else{
                    userDetailsWrapper = gson.fromJson(responseBody, UserDetailsWrapper.class);
                }
            } else {
                userDetailsWrapper = null;
            }
        }catch (Exception e){
            userDetailsWrapper = null;
        }
        return userDetailsWrapper;
    }

    public SuccessResponse updateUser(int userId, String apiKey, String userName, String email, String address) {
        SuccessResponse successResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.updateUser(userId, apiKey, userName, email, address);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    successResponse = null;
                }else{
                    successResponse = gson.fromJson(responseBody, SuccessResponse.class);
                }
            } else {
                successResponse = null;
            }
        }catch (Exception e){
            successResponse = null;
        }
        return successResponse;
    }

    public SuccessResponse checkOTP(String mobileNo) {
        SuccessResponse successResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkOTP(mobileNo);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    successResponse = null;
                }else{
                    successResponse = gson.fromJson(responseBody, SuccessResponse.class);
                }
            } else {
                successResponse = null;
            }
        }catch (Exception e){
            successResponse = null;
        }
        return successResponse;
    }

    public SuccessResponse changePassword(String mobileNo, String code, String pasword) {
        SuccessResponse successResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> change = mRepository.changePassword(mobileNo, code, pasword);

            Response<ResponseBody> response = change.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    successResponse = null;
                }else{
                    successResponse = gson.fromJson(responseBody, SuccessResponse.class);
                }
            } else {
                successResponse = null;
            }
        }catch (Exception e){
            successResponse = null;
        }
        return successResponse;
    }

    public AuctionVehicleWrapper fetchMyWins(int userId, String apiKey) {
        AuctionVehicleWrapper auctionVehicleWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchMyWins(userId, apiKey);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    auctionVehicleWrapper = null;
                }else{
                    auctionVehicleWrapper = gson.fromJson(responseBody, AuctionVehicleWrapper.class);
                }
            } else {
                auctionVehicleWrapper = null;
            }
        }catch (Exception e){
            auctionVehicleWrapper = null;
        }
        return auctionVehicleWrapper;
    }

    public UpcomingAuctionWrapper fetchUpcomings(int userId, String apiKey) {
        UpcomingAuctionWrapper upcomingAuctionWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchUpcomingAuction(userId, apiKey);

            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    upcomingAuctionWrapper = null;
                }else{
                    upcomingAuctionWrapper = gson.fromJson(responseBody, UpcomingAuctionWrapper.class);
                }
            } else {
                upcomingAuctionWrapper = null;
            }
        }catch (Exception e){
            upcomingAuctionWrapper = null;
        }
        return upcomingAuctionWrapper;
    }

}
