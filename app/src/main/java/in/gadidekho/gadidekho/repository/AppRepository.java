package in.gadidekho.gadidekho.repository;

import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Multipart;
import retrofit2.http.POST;
import retrofit2.http.Part;

public interface AppRepository {

    @Multipart
    @POST("user_registration")
    Call<ResponseBody> registerUser(
            @Part("members_name") RequestBody name,
            @Part("member_email") RequestBody email,
            @Part("mobile_no") RequestBody mobile,
            @Part("password") RequestBody password,
            @Part("address") RequestBody address,
            @Part MultipartBody.Part addressProofFile,
            @Part MultipartBody.Part idProofFile
            );

    @POST("user_login")
    @FormUrlEncoded
    Call<ResponseBody> checkLogin(@Field("user_name") String phoneNumber,
                                  @Field("password") String password
    );

    @POST("fetch_auction")
    @FormUrlEncoded
    Call<ResponseBody> fetchAuction(@Field("userId") int userId,
                                  @Field("api_token") String apiToken
    );

    @POST("fetch_auction_vehicle")
    @FormUrlEncoded
    Call<ResponseBody> fetchAuctionVehicles(@Field("userId") int userId,
                                    @Field("api_token") String apiToken,
                                    @Field("auction_id") int auctionId
    );

    @POST("vehicle_details")
    @FormUrlEncoded
    Call<ResponseBody> fetchVehicleDetails( @Field("userId") int userId,
                                            @Field("api_token") String apiToken,
                                            @Field("auction_id") int auctionId,
                                            @Field("vehicle_id") int vehicelId
    );

    @POST("bid")
    @FormUrlEncoded
    Call<ResponseBody> submitBid( @Field("userId") int userId,
                                            @Field("api_token") String apiToken,
                                            @Field("vehicle_id") int vehicelId,
                                            @Field("bid_amount") int bidAmount
    );

    @POST("all_notification")
    @FormUrlEncoded
    Call<ResponseBody> fetchNotifications( @Field("userId") int userId,
                                  @Field("api_token") String apiToken
    );

    @POST("your_bid")
    @FormUrlEncoded
    Call<ResponseBody> fetchMyBids( @Field("userId") int userId,
                                           @Field("api_token") String apiToken
    );

    @POST("payment_request_list")
    @FormUrlEncoded
    Call<ResponseBody> fetchMyPayments( @Field("userId") int userId,
                                    @Field("api_token") String apiToken
    );

    @POST("add_wish_list")
    @FormUrlEncoded
    Call<ResponseBody> addToWishList( @Field("userId") int userId,
                                  @Field("api_token") String apiToken,
                                  @Field("vehicleId") int vehicelId);

    @POST("wish_list")
    @FormUrlEncoded
    Call<ResponseBody> fetchWishList( @Field("userId") int userId,
                                      @Field("api_token") String apiToken);


    @POST("remove_wish_list")
    @FormUrlEncoded
    Call<ResponseBody> removeFromWishList( @Field("userId") int userId,
                                      @Field("api_token") String apiToken,
                                      @Field("vehicleId") int vehicelId);

    @POST("deposit_buying_limit_available")
    @FormUrlEncoded
    Call<ResponseBody> fetchDepositLimit( @Field("userId") int userId,
                                      @Field("api_token") String apiToken);

    @POST("member_data")
    @FormUrlEncoded
    Call<ResponseBody> fetchUserDetails( @Field("userId") int userId,
                                          @Field("api_token") String apiToken);

    @POST("update_member_data")
    @FormUrlEncoded
    Call<ResponseBody> updateUser( @Field("userId") int userId,
                                   @Field("api_token") String apiToken,
                                   @Field("userName") String userName,
                                   @Field("email") String email,
                                   @Field("address") String address
                                   );

    @POST("check_mobile_no")
    @FormUrlEncoded
    Call<ResponseBody> checkOTP( @Field("mobile_no") String mobileNo);

    @POST("set_password")
    @FormUrlEncoded
    Call<ResponseBody> changePassword( @Field("mobile_no") String mobileNo,
                                 @Field("confirm_code") String confirmCode,
                                 @Field("password") String password
                                 );

    @POST("win")
    @FormUrlEncoded
    Call<ResponseBody> fetchMyWins( @Field("userId") int userId,
                                      @Field("api_token") String apiToken);

    @POST("upcomming_auction")
    @FormUrlEncoded
    Call<ResponseBody> fetchUpcomingAuction( @Field("userId") int userId,
                                    @Field("api_token") String apiToken);

}
