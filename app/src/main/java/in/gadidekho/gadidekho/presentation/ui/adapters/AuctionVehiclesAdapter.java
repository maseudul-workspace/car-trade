package in.gadidekho.gadidekho.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.ViewPager;
import androidx.recyclerview.widget.RecyclerView;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.util.GlideHelper;

public class AuctionVehiclesAdapter extends RecyclerView.Adapter<AuctionVehiclesAdapter.ViewHolder> {

    public interface Callback {
        void onAuctionVehicleClicked(int id, int auctionId);
        void addToWishList(int id, int position);
        void removeFromWishList(int id, int position);
    }

    Context mContext;
    AuctionVehicle[] auctionVehicles;
    Callback mCallback;

    public AuctionVehiclesAdapter(Context mContext, AuctionVehicle[] auctionVehicles, Callback mCallback) {
        this.mContext = mContext;
        this.auctionVehicles = auctionVehicles;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_auction_vehicle, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewCurrentBids.setText(Integer.toString(auctionVehicles[i].current_bid_amount));
        viewHolder.txtViewFuelType.setText(auctionVehicles[i].fuel_type);
        viewHolder.txtViewManufactureyear.setText(auctionVehicles[i].mfg_month_year);
        viewHolder.txtViewOwnerType.setText(auctionVehicles[i].owner_type);
        viewHolder.txtViewRegistration.setText(auctionVehicles[i].registrationNo);
        viewHolder.txtViewRegistrationAvailable.setText(auctionVehicles[i].registrationAvailable);
        viewHolder.txtViewState.setText(auctionVehicles[i].state);
        viewHolder.txtViewTime.setText(auctionVehicles[i].time);
        viewHolder.txtViewTotalBids.setText(Integer.toString(auctionVehicles[i].total_remaining_bids));
        viewHolder.txtViewTransmisssiontype.setText(auctionVehicles[i].transmission_type);
        SliderImagesAdapter imagesAdapter = new SliderImagesAdapter(mContext, auctionVehicles[i].images);
        viewHolder.viewPager.setAdapter(imagesAdapter);
        viewHolder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onAuctionVehicleClicked(auctionVehicles[i].vehicleId, auctionVehicles[i].auctionId);
            }
        });

        if (auctionVehicles[i].isWishListPresent) {
            viewHolder.imgViewHeartFill.setVisibility(View.VISIBLE);
            viewHolder.imgViewHeartOutline.setVisibility(View.GONE);
        } else {
            viewHolder.imgViewHeartFill.setVisibility(View.GONE);
            viewHolder.imgViewHeartOutline.setVisibility(View.VISIBLE);
        }

        viewHolder.imgViewHeartFill.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.removeFromWishList(auctionVehicles[i].vehicleId, i);
            }
        });

        viewHolder.imgViewHeartOutline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.addToWishList(auctionVehicles[i].vehicleId, i);
            }
        });

        viewHolder.btnView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onAuctionVehicleClicked(auctionVehicles[i].vehicleId, auctionVehicles[i].auctionId);
            }
        });
        viewHolder.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareDotsIndicator(position, viewHolder.dotsIndicatorLayout, auctionVehicles[i].images.length);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        prepareDotsIndicator(0, viewHolder.dotsIndicatorLayout, auctionVehicles[i].images.length);
    }

    @Override
    public int getItemCount() {
        return auctionVehicles.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_registration)
        TextView txtViewRegistration;
        @BindView(R.id.txt_view_registration_available)
        TextView txtViewRegistrationAvailable;
        @BindView(R.id.txt_view_manufacture_year)
        TextView txtViewManufactureyear;
        @BindView(R.id.txt_view_fuel_type)
        TextView txtViewFuelType;
        @BindView(R.id.txt_view_owner_type)
        TextView txtViewOwnerType;
        @BindView(R.id.txt_view_state)
        TextView txtViewState;
        @BindView(R.id.txt_view_transmission_type)
        TextView txtViewTransmisssiontype;
        @BindView(R.id.txt_view_total_bids)
        TextView txtViewTotalBids;
        @BindView(R.id.txt_view_current_bid_amount)
        TextView txtViewCurrentBids;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.viewpager)
        ViewPager viewPager;
        @BindView(R.id.layout_main)
        View mainLayout;
        @BindView(R.id.img_view_heart_outline)
        ImageView imgViewHeartOutline;
        @BindView(R.id.img_view_heart_fill)
        ImageView imgViewHeartFill;
        @BindView(R.id.btn_view)
        Button btnView;
        @BindView(R.id.dots_indicator_layout)
        LinearLayout dotsIndicatorLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void onAddingWishListSuccess(int position) {
        auctionVehicles[position].isWishListPresent = true;
        notifyItemChanged(position);
    }

    public void onAddingWishListFail(int position) {
        auctionVehicles[position].isWishListPresent = false;
        notifyItemChanged(position);
    }

    public void prepareDotsIndicator(int sliderPosition, LinearLayout dotsLayout, int imageCount){
        if(dotsLayout.getChildCount() > 0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[imageCount];
        for(int i = 0; i < imageCount; i++){
            dots[i] = new ImageView(mContext);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(mContext, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(mContext, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(14, 14);
            layoutParams.setMargins(5, 0, 5, 0);
            dotsLayout.addView(dots[i], layoutParams);

        }
    }

}
