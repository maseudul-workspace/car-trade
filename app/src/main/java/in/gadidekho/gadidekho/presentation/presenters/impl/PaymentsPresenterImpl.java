package in.gadidekho.gadidekho.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchPaymentsInteractor;
import in.gadidekho.gadidekho.domain.interactors.impl.FetchPaymentsInteractorImpl;
import in.gadidekho.gadidekho.domain.models.Payments;
import in.gadidekho.gadidekho.domain.models.UserInfo;
import in.gadidekho.gadidekho.presentation.presenters.PaymentsPresenter;
import in.gadidekho.gadidekho.presentation.presenters.base.AbstractPresenter;
import in.gadidekho.gadidekho.presentation.ui.adapters.PaymentsAdapter;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class PaymentsPresenterImpl extends AbstractPresenter implements PaymentsPresenter, FetchPaymentsInteractor.Callback, PaymentsAdapter.Callback {

    Context mContext;
    PaymentsPresenter.View mView;
    FetchPaymentsInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    PaymentsAdapter adapter;

    public PaymentsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchPayments() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new FetchPaymentsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
        mInteractor.execute();
    }

    @Override
    public void onPaymentFetchSuccess(Payments[] payments) {
        mView.hideLoader();

        if (payments.length == 0) {
            Toasty.warning(mContext, "No Payments Found", Toast.LENGTH_SHORT, true).show();
        } else {
            adapter = new PaymentsAdapter(mContext, payments, this::onPaymentClicked);
            mView.loadAdapter(adapter);
        }
    }

    @Override
    public void onPaymentFetchFailed(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onPaymentClicked(int paymentId, double amount) {
        mView.goToPaymentActivity(paymentId, amount);
    }
}
