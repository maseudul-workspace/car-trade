package in.gadidekho.gadidekho.presentation.presenters;

import in.gadidekho.gadidekho.presentation.ui.adapters.MyWinsAdapter;

public interface FetchMyWinsPresenter {
    void fetchMyWins();
    interface View {
        void showLoader();
        void hideLoader();
        void loadAdapter(MyWinsAdapter adapter);
    }
}
