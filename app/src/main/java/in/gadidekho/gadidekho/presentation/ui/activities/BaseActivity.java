package in.gadidekho.gadidekho.presentation.ui.activities;

import android.content.Intent;

import androidx.annotation.IntRange;
import androidx.annotation.LayoutRes;
import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.FrameLayout;

import com.mikepenz.materialdrawer.AccountHeader;
import com.mikepenz.materialdrawer.AccountHeaderBuilder;
import com.mikepenz.materialdrawer.Drawer;
import com.mikepenz.materialdrawer.DrawerBuilder;
import com.mikepenz.materialdrawer.model.PrimaryDrawerItem;
import com.mikepenz.materialdrawer.model.ProfileDrawerItem;
import com.mikepenz.materialdrawer.model.interfaces.IDrawerItem;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.R;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar)
    androidx.appcompat.widget.Toolbar toolbar;
    Drawer drawer;
    AndroidApplication androidApplication;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    protected void inflateContent(@LayoutRes int inflatedResID) {
        setContentView(R.layout.activity_base);
        FrameLayout contentFrameLayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflatedResID, contentFrameLayout);
        ButterKnife.bind(this);
        setDrawer();
    }

    public void setDrawer() {
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);

        // Create the AccountHeader
        AccountHeader headerResult = new AccountHeaderBuilder()
                .withActivity(this)
                .addProfiles(new ProfileDrawerItem().withIcon(R.drawable.app_logo).withName("Gaadi Trade"))
                .withHeaderBackground(R.color.primary_dark)
                .build();

        drawer = new DrawerBuilder()
                .withActivity(this)
                .withToolbar(toolbar)
                .withAccountHeader(headerResult)
                .withHasStableIds(true)
                .withDisplayBelowStatusBar(false)
                .withActionBarDrawerToggleAnimated(true)
                .addDrawerItems(
                        new PrimaryDrawerItem().withName("Home").withSelectable(false).withIdentifier(1),
                        new PrimaryDrawerItem().withName("Upcoming Auctions").withSelectable(false).withIdentifier(9),
                        new PrimaryDrawerItem().withName("Notifications").withSelectable(false).withIdentifier(3),
                        new PrimaryDrawerItem().withName("My Bids").withSelectable(false).withIdentifier(4),
                        new PrimaryDrawerItem().withName("My Payments").withSelectable(false).withIdentifier(5),
                        new PrimaryDrawerItem().withName("My Wish List").withSelectable(false).withIdentifier(6),
                        new PrimaryDrawerItem().withName("My Wins").withSelectable(false).withIdentifier(8),
                        new PrimaryDrawerItem().withName("My Profile").withSelectable(false).withIdentifier(7),
                        new PrimaryDrawerItem().withName("Log Out").withSelectable(false).withIdentifier(2)
                        ).withOnDrawerItemClickListener(new Drawer.OnDrawerItemClickListener() {
                    @Override
                    public boolean onItemClick(View view, int position, IDrawerItem drawerItem) {
                        if (drawerItem != null) {
                            switch (((int) drawerItem.getIdentifier())) {
                                case 2:
                                    androidApplication = (AndroidApplication) getApplicationContext();
                                    androidApplication.setUserInfo(getApplicationContext(), null);
                                    Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
                                    startActivity(loginIntent);
                                    break;
                                case 3:
                                    Intent notificationIntent = new Intent(getApplicationContext(), NotificationsActivity.class);
                                    startActivity(notificationIntent);
                                    break;
                                case 4:
                                    Intent myBidsIntent = new Intent(getApplicationContext(), MyBidsActivity.class);
                                    startActivity(myBidsIntent);
                                    break;
                                case 5:
                                    Intent paymentsIntent = new Intent(getApplicationContext(), PaymentsActivity.class);
                                    startActivity(paymentsIntent);
                                    break;
                                case 6:
                                    Intent wishListIntent = new Intent(getApplicationContext(), WishListActivity.class);
                                    startActivity(wishListIntent);
                                    break;
                                case 7:
                                    Intent userProfileIntent = new Intent(getApplicationContext(), UserProfileActivity.class);
                                    startActivity(userProfileIntent);
                                    break;
                                case 8:
                                    Intent myWinsIntent = new Intent(getApplicationContext(), MyWinsActivity.class);
                                    startActivity(myWinsIntent);
                                    break;
                                case 9:
                                    Intent upcomingAuctionIntent = new Intent(getApplicationContext(), UpcomingAuctionsActivity.class);
                                    startActivity(upcomingAuctionIntent);
                                    break;
                            }
                        }
                        return false;
                    }
                }).build();
    }

}
