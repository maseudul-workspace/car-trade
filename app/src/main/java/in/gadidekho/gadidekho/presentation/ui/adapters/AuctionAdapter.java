package in.gadidekho.gadidekho.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.models.Auction;

public class AuctionAdapter extends RecyclerView.Adapter<AuctionAdapter.ViewHolder> {

    public interface Callback {
        void onAuctionClicked(int auctionId);
    }

    Context mContext;
    Auction[] auctions;
    Callback callback;

    public AuctionAdapter(Context mContext, Auction[] auctions, Callback callback) {
        this.mContext = mContext;
        this.auctions = auctions;
        this.callback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view = LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.recycler_view_auction, viewGroup, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, final int i) {
        viewHolder.txtViewAuction.setText(auctions[i].auctionGroupName);
        viewHolder.txtViewTotalVehicle.setText(auctions[i].totalVehicle);
        try {
            viewHolder.txtViewTime.setText(changeDateFormat(auctions[i].auction_end_date));
        } catch (ParseException e) {
            e.printStackTrace();
        }
        viewHolder.layoutMain.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callback.onAuctionClicked(auctions[i].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return auctions.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_auction_title)
        TextView txtViewAuction;
        @BindView(R.id.txt_view_total_vehicle)
        TextView txtViewTotalVehicle;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.layout_main)
        View layoutMain;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public String changeDateFormat(String dateString) throws ParseException {
        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date newDate = format.parse(dateString);
        format = new SimpleDateFormat("EEE, dd MMM hh:mm a");
        return format.format(newDate);
    }

}
