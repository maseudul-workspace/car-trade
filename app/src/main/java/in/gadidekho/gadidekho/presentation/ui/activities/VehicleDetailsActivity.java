package in.gadidekho.gadidekho.presentation.ui.activities;

import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.TextWatcher;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.executors.impl.ThreadExecutor;
import in.gadidekho.gadidekho.domain.models.VehicleDetails;
import in.gadidekho.gadidekho.presentation.presenters.FetchVehicleDetailsPresenter;
import in.gadidekho.gadidekho.presentation.presenters.impl.FetchVehicleDetailsPresenterImpl;
import in.gadidekho.gadidekho.presentation.ui.adapters.SliderImagesAdapter;
import in.gadidekho.gadidekho.threading.MainThreadImpl;

public class VehicleDetailsActivity extends AppCompatActivity implements FetchVehicleDetailsPresenter.View {

    @BindView(R.id.viewpager)
    ViewPager viewPager;
    @BindView(R.id.txt_view_vehicle_status)
    TextView txtViewStatus;
    @BindView(R.id.txt_vehicle_name)
    TextView txtViewVehicleName;
    @BindView(R.id.txt_view_registration)
    TextView txtViewRegistrationNo;
    @BindView(R.id.txt_view_registration_available)
    TextView txtViewRegistrationAvailable;
    @BindView(R.id.txt_view_manufacture_year)
    TextView txtViewManufactureYear;
    @BindView(R.id.txt_view_fuel_type)
    TextView txtViewFuelType;
    @BindView(R.id.txt_view_owner_type)
    TextView txtViewOwnerType;
    @BindView(R.id.txt_view_state)
    TextView txtViewState;
    @BindView(R.id.txt_view_transmission_type)
    TextView txtViewTransmissionType;
    @BindView(R.id.txt_view_total_bids)
    TextView txtViewTotalBids;
    @BindView(R.id.edit_text_current_bid_amount)
    EditText editTextCurrentBidAmount;
    @BindView(R.id.txt_view_bc_manufacture_year)
    TextView txtViewBCManufactureYear;
    @BindView(R.id.txt_view_bc_color)
    TextView txtViewBCColor;
    @BindView(R.id.txt_view_bc_engine_no)
    TextView txtViewBCEngineNo;
    @BindView(R.id.txt_view_bc_chasis_no)
    TextView txtViewBCChasisNo;
    @BindView(R.id.txt_view_bc_transmission_type)
    TextView txtViewBCTransissionType;
    @BindView(R.id.txt_view_bc_fuel_type)
    TextView txtViewBCFuelType;
    @BindView(R.id.txt_view_bc_owner_type)
    TextView txtViewBCOwnerType;
    @BindView(R.id.txt_view_bc_vehicle_type)
    TextView txtViewBCVehicleType;
    @BindView(R.id.txt_view_bc_ownership)
    TextView txtViewBCOwnership;
    @BindView(R.id.txt_view_rc_registration_available)
    TextView txtViewRCRegistrationAvailable;
    @BindView(R.id.txt_view_rc_registration)
    TextView txtViewRCRegistration;
    @BindView(R.id.txt_view_rc_registration_date)
    TextView txtViewRCRegistrationDate;
    @BindView(R.id.txt_view_rc_registration_as)
    TextView txtViewRCRegistrationAs;
    @BindView(R.id.txt_view_tx_expiry_date)
    TextView txtViewTXExpiryDate;
    @BindView(R.id.txt_view_tx_fitness_expiry_date)
    TextView txtViewTXFitnessExpiryDate;
    @BindView(R.id.txt_view_tx_permit_expiry_date)
    TextView txtViewTXPermitExpiryDate;
    @BindView(R.id.txt_view_tx_permit_type)
    TextView txtViewTXPermitType;
    @BindView(R.id.txt_view_tx_road_validity)
    TextView txtViewTXRoadTaxValidity;
    @BindView(R.id.txt_view_hi_car_under_hypothecation)
    TextView txtViewHICarUnderHypo;
    @BindView(R.id.txt_view_hi_financer_name)
    TextView txtViewHIFinancerName;
    @BindView(R.id.txt_view_hi_noc_available)
    TextView txtViewHINocAvailable;
    @BindView(R.id.txt_view_hi_repo_date)
    TextView txtViewHIRepoDate;
    @BindView(R.id.txt_view_hi_loan_paid_off)
    TextView txtViewHILoanPaidOff;
    @BindView(R.id.txt_view_li_zone)
    TextView txtViewLIZone;
    @BindView(R.id.txt_view_li_state)
    TextView txtViewLIState;
    @BindView(R.id.txt_view_li_city)
    TextView txtViewLICity;
    @BindView(R.id.txt_view_li_yard_name)
    TextView txtViewLIYardName;
    @BindView(R.id.txt_view_li_yard_location)
    TextView txtViewLIYardLocation;
    @BindView(R.id.txt_view_avi_superdari_status)
    TextView txtViewAVISuperdariStatus;
    @BindView(R.id.txt_view_avi_tax_type)
    TextView txtViewAVITaxType;
    @BindView(R.id.txt_view_avi_theft_recover)
    TextView txtViewTheftRecover;
    @BindView(R.id.txt_view_avi_keys_available)
    TextView txtViewAVIKeysAvailable;
    @BindView(R.id.txt_view_summary)
    TextView txtViewSummary;
    int auctionId;
    int vehicleId;
    FetchVehicleDetailsPresenterImpl mPresenter;
    int current_bid_amount;
    int remaining_bids;
    int user_bid_amount;
    @BindView(R.id.img_view_heart_fill)
    ImageView imgViewHeartFill;
    @BindView(R.id.img_view_heart_outline)
    ImageView imgViewHeartOutline;
    @BindView(R.id.swipe_refresh_layout)
    SwipeRefreshLayout swipeRefreshLayout;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vehicle_details);
        ButterKnife.bind(this);
        initialisePresenter();
        auctionId = getIntent().getIntExtra("auctionId", 0);
        vehicleId = getIntent().getIntExtra("vehicleId", 0);
        mPresenter.fetchVehicleDetails(auctionId, vehicleId);
        setUpSwipeRefreshLayout();
        setUpProgressDialog();
        showLoader();
        getSupportActionBar().setTitle("Vehicle Details");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setUpSwipeRefreshLayout() {
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mPresenter.fetchVehicleDetails(auctionId, vehicleId);

            }
        });
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void initialisePresenter() {
        mPresenter = new FetchVehicleDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadData(VehicleDetails vehicleDetails) {
        SliderImagesAdapter sliderImagesAdapter = new SliderImagesAdapter(this, vehicleDetails.images);
        viewPager.setAdapter(sliderImagesAdapter);
        txtViewAVIKeysAvailable.setText(vehicleDetails.avi_keys_available);
        txtViewAVISuperdariStatus.setText(vehicleDetails.avi_superdari_status);
        txtViewAVITaxType.setText(vehicleDetails.avi_tax_type);
        txtViewBCChasisNo.setText(vehicleDetails.bc_chasis_no);
        txtViewBCColor.setText(vehicleDetails.bc_color);
        txtViewBCEngineNo.setText(vehicleDetails.bc_engine_no);
        txtViewBCFuelType.setText(vehicleDetails.bc_fuel_type);
        txtViewBCManufactureYear.setText(vehicleDetails.bc_mfg_month_year);
        txtViewBCOwnership.setText(vehicleDetails.bc_ownership);
        txtViewBCOwnerType.setText(vehicleDetails.bc_owner_type);
        txtViewBCTransissionType.setText(vehicleDetails.bc_transmission_type);
        txtViewBCVehicleType.setText(vehicleDetails.bc_vehicle_type);
        current_bid_amount = vehicleDetails.current_bid_amount;
        user_bid_amount = current_bid_amount;
        editTextCurrentBidAmount.setText(Integer.toString(current_bid_amount));
        remaining_bids = vehicleDetails.total_remaining_bids;
        txtViewFuelType.setText(vehicleDetails.fuel_type);
        txtViewHICarUnderHypo.setText(vehicleDetails.hi_car_under_hypothecation);
        txtViewHIFinancerName.setText(vehicleDetails.hi_financer_name);
        txtViewHILoanPaidOff.setText(vehicleDetails.hi_loan_paid_off);
        txtViewHINocAvailable.setText(vehicleDetails.hi_noc_available);
        txtViewHIRepoDate.setText(vehicleDetails.hi_repo_date);
        txtViewLICity.setText(vehicleDetails.li_city);
        txtViewLIState.setText(vehicleDetails.li_state);
        txtViewLIYardLocation.setText(vehicleDetails.li_yard_location);
        txtViewLIYardName.setText(vehicleDetails.li_yard_name);
        txtViewLIZone.setText(vehicleDetails.li_zone);
        txtViewManufactureYear.setText(vehicleDetails.mfg_month_year);
        txtViewOwnerType.setText(vehicleDetails.owner_type);
        txtViewRCRegistration.setText(vehicleDetails.rc_registration_no);
        txtViewRCRegistrationAs.setText(vehicleDetails.rc_reg_as);
        txtViewRCRegistrationAvailable.setText(vehicleDetails.rc_rc_available);
        txtViewRCRegistrationDate.setText(vehicleDetails.rc_registration_date);
        txtViewRegistrationNo.setText(vehicleDetails.registrationNo);
        txtViewState.setText(vehicleDetails.state);
        txtViewStatus.setText(vehicleDetails.status);
        txtViewSummary.setText(Html.fromHtml(vehicleDetails.summary));
        txtViewTheftRecover.setText(vehicleDetails.avi_theft_recover);
        txtViewTotalBids.setText(Integer.toString(vehicleDetails.total_remaining_bids));
        txtViewTransmissionType.setText(vehicleDetails.transmission_type);
        txtViewTXExpiryDate.setText(vehicleDetails.tx_road_text_expiray_date);
        txtViewTXFitnessExpiryDate.setText(vehicleDetails.tx_fitness_expiray_date);
        txtViewTXPermitExpiryDate.setText(vehicleDetails.tx_permit_expiray_date);
        txtViewTXPermitType.setText(vehicleDetails.tx_permit_type);
        txtViewTXRoadTaxValidity.setText(vehicleDetails.tx_road_taxt_validity);
        txtViewVehicleName.setText(vehicleDetails.vehicleName);
        txtViewRegistrationAvailable.setText(vehicleDetails.registrationAvailable);
        if (vehicleDetails.isWishListPresent) {
            imgViewHeartFill.setVisibility(View.VISIBLE);
        } else {
            imgViewHeartOutline.setVisibility(View.VISIBLE);
        }

        if (vehicleDetails.status.equals("Start bidding now !!")) {
            txtViewStatus.setTextColor(getResources().getColor(R.color.blue1));
        } else if (vehicleDetails.status.equals("Lossing !!")) {
            txtViewStatus.setTextColor(getResources().getColor(R.color.red1));
        } else {
            txtViewStatus.setTextColor(getResources().getColor(R.color.green1));
        }

    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void onWishListAddingSuccess() {
        imgViewHeartFill.setVisibility(View.VISIBLE);
        imgViewHeartOutline.setVisibility(View.GONE);
    }

    @Override
    public void onWishListRemovingSuccess() {
        imgViewHeartFill.setVisibility(View.GONE);
        imgViewHeartOutline.setVisibility(View.VISIBLE);
    }

    @Override
    public void stopRefreshing() {
        if (swipeRefreshLayout.isRefreshing()) {
            swipeRefreshLayout.setRefreshing(false);
        }
    }


    @OnClick(R.id.btn_low_bid) void onLowBidClicked() {
        if (Integer.parseInt(editTextCurrentBidAmount.getText().toString()) > current_bid_amount) {
            editTextCurrentBidAmount.setText(Integer.toString(Integer.parseInt(editTextCurrentBidAmount.getText().toString()) - 1000));
        }
    }

    @OnClick(R.id.btn_up_bid) void onUpBidClicked() {
        editTextCurrentBidAmount.setText(Integer.toString(Integer.parseInt(editTextCurrentBidAmount.getText().toString()) + 1000));
    }

    @OnClick(R.id.btn_bid) void onBidClicked() {
        if (!editTextCurrentBidAmount.getText().toString().trim().isEmpty()) {
            mPresenter.submitBid(vehicleId, Integer.parseInt(editTextCurrentBidAmount.getText().toString()));
            showLoader();
        } else {
            Toasty.warning(this, "Please enter bid amount", Toast.LENGTH_SHORT, true).show();
        }
    }

    @OnClick(R.id.img_view_heart_outline) void addToWishList() {
        mPresenter.addToWishlist();
    }

    @OnClick(R.id.img_view_heart_fill) void removeFromWishList() {
        mPresenter.removeFromWishlist();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
