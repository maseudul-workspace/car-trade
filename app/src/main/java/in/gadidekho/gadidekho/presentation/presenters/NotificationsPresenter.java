package in.gadidekho.gadidekho.presentation.presenters;

import in.gadidekho.gadidekho.presentation.ui.adapters.NotificationsAdapter;

public interface NotificationsPresenter {
    void fetchNotifications();
    interface View {
        void loadAdapter(NotificationsAdapter notificationsAdapter);
        void showLoader();
        void hideLoader();

    }
}
