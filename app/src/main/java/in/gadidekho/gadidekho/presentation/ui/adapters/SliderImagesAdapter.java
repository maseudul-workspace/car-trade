package in.gadidekho.gadidekho.presentation.ui.adapters;

import android.content.Context;
import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import in.gadidekho.gadidekho.domain.models.Images;
import in.gadidekho.gadidekho.util.GlideHelper;

public class SliderImagesAdapter extends PagerAdapter {

    Context mContext;
    Images[] vehicleImages;

    public SliderImagesAdapter(Context mContext, Images[] vehicleImages) {
        this.mContext = mContext;
        this.vehicleImages = vehicleImages;
    }

    @Override
    public int getCount() {
        return vehicleImages.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        ImageView imageView = new ImageView(mContext);
        imageView.setScaleType(ImageView.ScaleType.FIT_XY);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imageView, vehicleImages[position].img, 10);
        container.addView(imageView);
        return imageView;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }
}
