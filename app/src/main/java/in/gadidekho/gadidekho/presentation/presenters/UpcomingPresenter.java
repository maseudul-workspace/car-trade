package in.gadidekho.gadidekho.presentation.presenters;

import in.gadidekho.gadidekho.presentation.ui.adapters.UpcomingAuctionAdapter;

public interface UpcomingPresenter {
    void fetchMyUpcomingAuctions();
    interface View {
        void loadAdapter(UpcomingAuctionAdapter adapter);
        void showLoader();
        void hideLoader();
    }

}
