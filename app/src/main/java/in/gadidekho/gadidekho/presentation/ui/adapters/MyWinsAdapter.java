package in.gadidekho.gadidekho.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.util.GlideHelper;

public class MyWinsAdapter extends RecyclerView.Adapter<MyWinsAdapter.ViewHolder> {

    Context mContext;
    AuctionVehicle[] auctionVehicles;

    public MyWinsAdapter(Context mContext, AuctionVehicle[] auctionVehicles) {
        this.mContext = mContext;
        this.auctionVehicles = auctionVehicles;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_my_wins, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int i) {
        viewHolder.txtViewFuelType.setText(auctionVehicles[i].fuel_type);
        viewHolder.txtViewManufactureyear.setText(auctionVehicles[i].mfg_month_year);
        viewHolder.txtViewOwnerType.setText(auctionVehicles[i].owner_type);
        viewHolder.txtViewRegistration.setText(auctionVehicles[i].registrationNo);
        viewHolder.txtViewRegistrationAvailable.setText(auctionVehicles[i].registrationAvailable);
        viewHolder.txtViewState.setText(auctionVehicles[i].state);
        viewHolder.txtViewTime.setText(auctionVehicles[i].time);
        viewHolder.txtViewTransmisssiontype.setText(auctionVehicles[i].transmission_type);
        SliderImagesAdapter imagesAdapter = new SliderImagesAdapter(mContext, auctionVehicles[i].images);
        viewHolder.viewPager.setAdapter(imagesAdapter);
        viewHolder.viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                prepareDotsIndicator(position, viewHolder.dotsIndicatorLayout, auctionVehicles[i].images.length);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        prepareDotsIndicator(0, viewHolder.dotsIndicatorLayout, auctionVehicles[i].images.length);
    }

    @Override
    public int getItemCount() {
        return auctionVehicles.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_registration)
        TextView txtViewRegistration;
        @BindView(R.id.txt_view_registration_available)
        TextView txtViewRegistrationAvailable;
        @BindView(R.id.txt_view_manufacture_year)
        TextView txtViewManufactureyear;
        @BindView(R.id.txt_view_fuel_type)
        TextView txtViewFuelType;
        @BindView(R.id.txt_view_owner_type)
        TextView txtViewOwnerType;
        @BindView(R.id.txt_view_state)
        TextView txtViewState;
        @BindView(R.id.txt_view_transmission_type)
        TextView txtViewTransmisssiontype;
        @BindView(R.id.txt_view_time)
        TextView txtViewTime;
        @BindView(R.id.viewpager)
        ViewPager viewPager;
        @BindView(R.id.layout_main)
        View mainLayout;
        @BindView(R.id.dots_indicator_layout)
        LinearLayout dotsIndicatorLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void prepareDotsIndicator(int sliderPosition, LinearLayout dotsLayout, int imageCount){
        if(dotsLayout.getChildCount() > 0){
            dotsLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[imageCount];
        for(int i = 0; i < imageCount; i++){
            dots[i] = new ImageView(mContext);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(mContext, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(mContext, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(12, 12);
            layoutParams.setMargins(5, 0, 5, 0);
            dotsLayout.addView(dots[i], layoutParams);

        }
    }

}
