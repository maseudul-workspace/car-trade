package in.gadidekho.gadidekho.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.executors.impl.ThreadExecutor;
import in.gadidekho.gadidekho.presentation.presenters.UpcomingPresenter;
import in.gadidekho.gadidekho.presentation.presenters.impl.UpcomingPresenterImpl;
import in.gadidekho.gadidekho.presentation.ui.adapters.UpcomingAuctionAdapter;
import in.gadidekho.gadidekho.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;

public class UpcomingAuctionsActivity extends AppCompatActivity implements UpcomingPresenter.View {

    @BindView(R.id.recycler_view_my_upcoming_wins)
    RecyclerView recyclerViewMyUpcomingWins;
    UpcomingPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upcoming_auctions);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Upcoming Auctions");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setProgressDialog();
        mPresenter.fetchMyUpcomingAuctions();
        showLoader();
    }

    public void initialisePresenter() {
        mPresenter = new UpcomingPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(UpcomingAuctionAdapter adapter) {
        recyclerViewMyUpcomingWins.setAdapter(adapter);
        recyclerViewMyUpcomingWins.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
