package in.gadidekho.gadidekho.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.executors.impl.ThreadExecutor;
import in.gadidekho.gadidekho.domain.models.UserDetails;
import in.gadidekho.gadidekho.presentation.presenters.UserProfilePresenter;
import in.gadidekho.gadidekho.presentation.presenters.impl.UserProfilePresenterImpl;
import in.gadidekho.gadidekho.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

public class UserProfileActivity extends AppCompatActivity implements UserProfilePresenter.View {

    @BindView(R.id.edit_address)
    EditText editTextAddress;
    @BindView(R.id.edit_email)
    EditText editTextEmail;
    @BindView(R.id.edit_name)
    EditText editTextName;
    ProgressDialog progressDialog;
    @BindView(R.id.txt_view_phone)
    TextView txtViewPhone;
    UserProfilePresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        ButterKnife.bind(this);
        setUpProgressDialog();
        initialisePresenter();
        mPresenter.fetchUserDetails();
        showLoader();
        getSupportActionBar().setTitle("My Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void initialisePresenter() {
        mPresenter = new UserProfilePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadData(UserDetails userDetails) {
        editTextName.setText(userDetails.userName);
        editTextEmail.setText(userDetails.email);
        editTextAddress.setText(userDetails.address);
        txtViewPhone.setText(userDetails.mobileNo);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @OnClick(R.id.btn_register) void onUpdateClicked() {
        if (editTextAddress.getText().toString().trim().isEmpty() || editTextEmail.getText().toString().trim().isEmpty() || editTextName.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please all the fields", Toast.LENGTH_SHORT, true).show();
        } else {
            mPresenter.updateUser(editTextName.getText().toString(), editTextEmail.getText().toString(), editTextAddress.getText().toString());
            showLoader();
        }
    }

}
