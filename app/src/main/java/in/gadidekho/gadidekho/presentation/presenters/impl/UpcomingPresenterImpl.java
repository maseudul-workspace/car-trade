package in.gadidekho.gadidekho.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchMyUpcomingAuctionsInteracator;
import in.gadidekho.gadidekho.domain.interactors.impl.FetchMyUpcomingAuctionInteractorImpl;
import in.gadidekho.gadidekho.domain.models.UpcomingAuction;
import in.gadidekho.gadidekho.domain.models.UserInfo;
import in.gadidekho.gadidekho.presentation.presenters.UpcomingPresenter;
import in.gadidekho.gadidekho.presentation.presenters.base.AbstractPresenter;
import in.gadidekho.gadidekho.presentation.ui.adapters.UpcomingAuctionAdapter;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class UpcomingPresenterImpl extends AbstractPresenter implements UpcomingPresenter, FetchMyUpcomingAuctionsInteracator.Callback {

    Context mContext;
    UpcomingPresenter.View mView;
    FetchMyUpcomingAuctionInteractorImpl fetchMyUpcomingAuctionInteractor;
    AndroidApplication androidApplication;

    public UpcomingPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMyUpcomingAuctions() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchMyUpcomingAuctionInteractor = new FetchMyUpcomingAuctionInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.userId, userInfo.apiToken);
        fetchMyUpcomingAuctionInteractor.execute();
    }

    @Override
    public void onFetchingMyUpcomingAuctionsSuccess(UpcomingAuction[] upcomingAuctions) {
        UpcomingAuctionAdapter upcomingAuctionAdapter = new UpcomingAuctionAdapter(mContext, upcomingAuctions);
        mView.loadAdapter(upcomingAuctionAdapter);
        mView.hideLoader();
    }

    @Override
    public void onFetchingMyUpcomingAuctionsFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
        mView.hideLoader();
    }
}
