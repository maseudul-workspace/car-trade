package in.gadidekho.gadidekho.presentation.presenters;

import in.gadidekho.gadidekho.presentation.ui.adapters.AuctionVehiclesAdapter;

public interface FetchAuctionVehiclesPresenter {
    void fetchVehicles(int auctionId);
    interface View {
        void loadAdapter(AuctionVehiclesAdapter adapter);
        void goToVehicleDetails(int vehicleId);
        void showLoader();
        void hideLoader();
    }
}
