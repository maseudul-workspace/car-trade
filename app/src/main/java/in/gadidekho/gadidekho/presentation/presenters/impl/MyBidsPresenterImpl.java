package in.gadidekho.gadidekho.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.AddToWishListInteractor;
import in.gadidekho.gadidekho.domain.interactors.FetchMyBidsInteractor;
import in.gadidekho.gadidekho.domain.interactors.FetchWishListInteractor;
import in.gadidekho.gadidekho.domain.interactors.RemoveFromWishlistInteractor;
import in.gadidekho.gadidekho.domain.interactors.impl.AddToWishListInteractorImpl;
import in.gadidekho.gadidekho.domain.interactors.impl.FetchMyBidsInteractorImpl;
import in.gadidekho.gadidekho.domain.interactors.impl.FetchWishListInteractorImpl;
import in.gadidekho.gadidekho.domain.interactors.impl.RemoveFromWishlistInteractorImpl;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.UserInfo;
import in.gadidekho.gadidekho.presentation.presenters.MyBidsPresenter;
import in.gadidekho.gadidekho.presentation.presenters.base.AbstractPresenter;
import in.gadidekho.gadidekho.presentation.ui.adapters.AuctionVehiclesAdapter;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class MyBidsPresenterImpl extends AbstractPresenter implements MyBidsPresenter,
                                                                        FetchMyBidsInteractor.Callback,
                                                                        AuctionVehiclesAdapter.Callback,
                                                                        AddToWishListInteractor.Callback,
                                                                        RemoveFromWishlistInteractor.Callback,
                                                                        FetchWishListInteractor.Callback
                                                                        {

    Context mContext;
    MyBidsPresenter.View mView;
    FetchMyBidsInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    AuctionVehiclesAdapter adapter;
    int position;
    AddToWishListInteractorImpl addToWishListInteractor;
    RemoveFromWishlistInteractorImpl removeFromWishlistInteractor;
    FetchWishListInteractorImpl fetchWishListInteractor;

    public MyBidsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMyBids() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new FetchMyBidsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
        mInteractor.execute();
    }

    @Override
    public void onGettingMyBidsSuccess(AuctionVehicle[] auctionVehicles) {
        mView.hideLoader();
        if (auctionVehicles.length == 0) {
            Toasty.warning(mContext, "No Bids Found", Toast.LENGTH_SHORT, true).show();
        } else {
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            AuctionVehicle[] wishLists = androidApplication.getWishLists(mContext);

            if (wishLists != null) {
                for (int i = 0; i < auctionVehicles.length; i++) {
                    for (int j = 0; j < wishLists.length; j++) {
                        if (auctionVehicles[i].vehicleId == wishLists[j].vehicleId) {
                            auctionVehicles[i].isWishListPresent = true;
                            break;
                        } else {
                            auctionVehicles[i].isWishListPresent = false;
                        }
                    }
                }
            }
            adapter = new AuctionVehiclesAdapter(mContext, auctionVehicles, this);
            mView.loadAdapter(adapter);
        }
    }

    @Override
    public void onGettingMyBidsFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.hideLoader();
    }

    @Override
    public void onAuctionVehicleClicked(int id, int auctionId) {
        mView.goToProductDetails(id, auctionId);
    }

    @Override
    public void addToWishList(int id, int position) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        this.position = position;
        addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken, id);
        addToWishListInteractor.execute();
    }

    @Override
    public void removeFromWishList(int id, int position) {
        this.position = position;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        removeFromWishlistInteractor = new RemoveFromWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken, id);
        removeFromWishlistInteractor.execute();
    }

    void fetchWishList() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
        fetchWishListInteractor.execute();
    }

    @Override
    public void onAddToWishListSuccess() {
        Toasty.success(mContext, "Successfully added to wishlist", Toast.LENGTH_SHORT, true).show();
        adapter.onAddingWishListSuccess(position);
        fetchWishlist();
    }

    @Override
    public void onAddToWishListFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onRemoveFromWishListSuccess() {
        Toasty.success(mContext, "Successfully removed from wishlist", Toast.LENGTH_SHORT, true).show();
        adapter.onAddingWishListFail(position);
        fetchWishlist();
    }

    @Override
    public void onRemoveFromWishListFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    public void fetchWishlist() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
        fetchWishListInteractor.execute();
    }

    @Override
    public void onGettingWishListSuccess(AuctionVehicle[] auctionVehicles) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(mContext, auctionVehicles);
    }

    @Override
    public void onGettingWishListFail(String errorMsg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(mContext, null);
    }
}
