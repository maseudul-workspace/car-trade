package in.gadidekho.gadidekho.presentation.presenters;

import in.gadidekho.gadidekho.domain.models.UserDetails;

public interface UserProfilePresenter {
    void fetchUserDetails();
    void updateUser(String userName, String email, String address);
    interface View {
        void loadData(UserDetails userDetails);
        void showLoader();
        void hideLoader();
    }
}
