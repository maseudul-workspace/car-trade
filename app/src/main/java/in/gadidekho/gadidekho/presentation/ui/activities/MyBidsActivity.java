package in.gadidekho.gadidekho.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.executors.impl.ThreadExecutor;
import in.gadidekho.gadidekho.presentation.presenters.MyBidsPresenter;
import in.gadidekho.gadidekho.presentation.presenters.impl.MyBidsPresenterImpl;
import in.gadidekho.gadidekho.presentation.ui.adapters.AuctionVehiclesAdapter;
import in.gadidekho.gadidekho.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

public class MyBidsActivity extends AppCompatActivity implements MyBidsPresenter.View {

    @BindView(R.id.recycler_view_my_bids)
    RecyclerView recyclerViewMyBids;
    MyBidsPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_bids);
        ButterKnife.bind(this);
        initialisePresenter();
        getSupportActionBar().setTitle("My Bids");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setUpProgressDialog();
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void initialisePresenter() {
        mPresenter = new MyBidsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadAdapter(AuctionVehiclesAdapter adapter) {
        recyclerViewMyBids.setAdapter(adapter);
        recyclerViewMyBids.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void goToProductDetails(int vehicleId, int auctionId) {
        Intent intent = new Intent(this, VehicleDetailsActivity.class);
        intent.putExtra("auctionId", auctionId);
        intent.putExtra("vehicleId", vehicleId);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchMyBids();
        showLoader();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
