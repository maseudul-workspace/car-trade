package in.gadidekho.gadidekho.presentation.presenters;

import in.gadidekho.gadidekho.presentation.ui.adapters.AuctionVehiclesAdapter;

public interface WishListPresenter {
    void fetchWishList();
    interface View {
        void loadAdapter(AuctionVehiclesAdapter adapter);
        void showLoader();
        void hideLoader();
        void goToProductDetails(int vehicleId, int auctionId);
    }
}
