package in.gadidekho.gadidekho.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchAuctionsInteractor;
import in.gadidekho.gadidekho.domain.interactors.FetchDepositLimitInteractor;
import in.gadidekho.gadidekho.domain.interactors.FetchWishListInteractor;
import in.gadidekho.gadidekho.domain.interactors.impl.FetchAuctionInteractorImpl;
import in.gadidekho.gadidekho.domain.interactors.impl.FetchDepositLimitInteractorImpl;
import in.gadidekho.gadidekho.domain.interactors.impl.FetchWishListInteractorImpl;
import in.gadidekho.gadidekho.domain.models.Auction;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.DepositLimit;
import in.gadidekho.gadidekho.domain.models.UserInfo;
import in.gadidekho.gadidekho.presentation.presenters.MainPresenter;
import in.gadidekho.gadidekho.presentation.presenters.WishListPresenter;
import in.gadidekho.gadidekho.presentation.presenters.base.AbstractPresenter;
import in.gadidekho.gadidekho.presentation.ui.adapters.AuctionAdapter;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter, FetchAuctionsInteractor.Callback, AuctionAdapter.Callback, FetchWishListInteractor.Callback, FetchDepositLimitInteractor.Callback {

    Context mContext;
    MainPresenter.View mView;
    FetchAuctionInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    AuctionAdapter auctionAdapter;
    FetchWishListInteractorImpl fetchWishListInteractor;
    FetchDepositLimitInteractorImpl fetchDepositLimitInteractor;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchAuctions() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new FetchAuctionInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
        mInteractor.execute();
    }

    @Override
    public void fetchWishlist() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
        fetchWishListInteractor.execute();
    }

    @Override
    public void fetchDepositLimit() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchDepositLimitInteractor = new FetchDepositLimitInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
        fetchDepositLimitInteractor.execute();
    }

    @Override
    public void onFetchAuctionSuccess(Auction[] auctions) {
        mView.hideLoader();
        if (auctions.length == 0) {
            Toasty.warning(mContext, "No Auction Available", Toast.LENGTH_SHORT, true).show();
        } else {
            auctionAdapter = new AuctionAdapter(mContext, auctions, this);
            mView.loadAdapter(auctionAdapter);
        }
    }

    @Override
    public void onFetchAuctionFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.hideLoader();
    }

    @Override
    public void onAuctionClicked(int auctionId) {
        mView.goToAuctionDetails(auctionId);
    }

    @Override
    public void onGettingWishListSuccess(AuctionVehicle[] auctionVehicles) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(mContext, auctionVehicles);
    }

    @Override
    public void onGettingWishListFail(String errorMsg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(mContext, null);
    }

    @Override
    public void onGettingDepositLimitSuccess(DepositLimit depositLimit) {
        mView.loadDepositLimit(depositLimit);
    }

    @Override
    public void onGettingDepositLimitFail(String errorMsg) {

    }
}
