package in.gadidekho.gadidekho.presentation.presenters;

public interface RegisterPresenter {
    void register(String name, String email, String mobile, String password, String address, String addressProofFile, String idProofFile);
    interface View {
        void hideLoader();
        void clearData();
    }
}
