package in.gadidekho.gadidekho.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.AddToWishListInteractor;
import in.gadidekho.gadidekho.domain.interactors.FetchVehicleDetailsInteractor;
import in.gadidekho.gadidekho.domain.interactors.FetchWishListInteractor;
import in.gadidekho.gadidekho.domain.interactors.RemoveFromWishlistInteractor;
import in.gadidekho.gadidekho.domain.interactors.SubmitBidInteractor;
import in.gadidekho.gadidekho.domain.interactors.impl.AddToWishListInteractorImpl;
import in.gadidekho.gadidekho.domain.interactors.impl.FetchVehicleDetailsInteractorImpl;
import in.gadidekho.gadidekho.domain.interactors.impl.FetchWishListInteractorImpl;
import in.gadidekho.gadidekho.domain.interactors.impl.RemoveFromWishlistInteractorImpl;
import in.gadidekho.gadidekho.domain.interactors.impl.SubmitBidInteractorImpl;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.UserInfo;
import in.gadidekho.gadidekho.domain.models.VehicleDetails;
import in.gadidekho.gadidekho.presentation.presenters.FetchVehicleDetailsPresenter;
import in.gadidekho.gadidekho.presentation.presenters.base.AbstractPresenter;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class FetchVehicleDetailsPresenterImpl extends AbstractPresenter implements FetchVehicleDetailsPresenter,
                                                                                    FetchVehicleDetailsInteractor.Callback,
                                                                                    SubmitBidInteractor.Callback,
                                                                                    AddToWishListInteractor.Callback,
                                                                                    RemoveFromWishlistInteractor.Callback,
                                                                                    FetchWishListInteractor.Callback
                                                                                    {

    Context mContext;
    FetchVehicleDetailsPresenter.View mView;
    FetchVehicleDetailsInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    SubmitBidInteractorImpl submitBidInteractor;
    int auctionId;
    int vehicleId;
    AddToWishListInteractorImpl addToWishListInteractor;
    RemoveFromWishlistInteractorImpl removeFromWishlistInteractor;
    FetchWishListInteractorImpl fetchWishListInteractor;


    public FetchVehicleDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchVehicleDetails(int auctionId, int vehicleId) {
        this.auctionId = auctionId;
        this.vehicleId =vehicleId;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new FetchVehicleDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken, auctionId, vehicleId);
        mInteractor.execute();
    }

    @Override
    public void submitBid(int vehicleId, int bid) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        submitBidInteractor = new SubmitBidInteractorImpl(mExecutor,mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken, vehicleId, bid);
        submitBidInteractor.execute();
    }

    @Override
    public void addToWishlist() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken, vehicleId);
        addToWishListInteractor.execute();
    }

    @Override
    public void removeFromWishlist() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        removeFromWishlistInteractor = new RemoveFromWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken, vehicleId);
        removeFromWishlistInteractor.execute();
    }

    @Override
    public void onGettingVehicleDetailsSuccess(VehicleDetails vehicleDetails) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        AuctionVehicle[] wishList = androidApplication.getWishLists(mContext);
        if (wishList != null) {
            for (int i = 0; i < wishList.length; i++) {
                if (wishList[i].vehicleId == vehicleDetails.vehicleId) {
                    vehicleDetails.isWishListPresent = true;
                    break;
                }
            }
        }
        mView.hideLoader();
        mView.loadData(vehicleDetails);
        mView.stopRefreshing();
    }

    @Override
    public void onGettingVehicleDetailsFail(String errorMsg) {
        mView.hideLoader();
        mView.stopRefreshing();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onBiddingSuccess() {
        Toasty.success(mContext, "Bid placed successfully", Toast.LENGTH_SHORT, true).show();
        fetchVehicleDetails(this.auctionId, this.vehicleId);
        mView.hideLoader();
    }

    @Override
    public void onBiddingFail() {
        Toasty.error(mContext, "Something went wrong", Toast.LENGTH_SHORT, true).show();
        mView.hideLoader();
    }

    @Override
    public void onAddToWishListSuccess() {
        Toasty.success(mContext, "Successfully added to wishlist", Toast.LENGTH_SHORT, true).show();
        mView.onWishListAddingSuccess();
        fetchWishlist();
    }

    @Override
    public void onAddToWishListFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onRemoveFromWishListSuccess() {
        Toasty.success(mContext, "Successfully removed from wishlist", Toast.LENGTH_SHORT, true).show();
        mView.onWishListRemovingSuccess();
        fetchWishlist();
    }

    @Override
    public void onRemoveFromWishListFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    public void fetchWishlist() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
        fetchWishListInteractor.execute();
    }

    @Override
    public void onGettingWishListSuccess(AuctionVehicle[] auctionVehicles) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(mContext, auctionVehicles);
    }

    @Override
    public void onGettingWishListFail(String errorMsg) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(mContext, null);
    }
}
