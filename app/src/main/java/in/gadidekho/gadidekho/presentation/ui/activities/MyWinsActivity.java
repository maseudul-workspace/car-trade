package in.gadidekho.gadidekho.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.executors.impl.ThreadExecutor;
import in.gadidekho.gadidekho.presentation.presenters.FetchMyWinsPresenter;
import in.gadidekho.gadidekho.presentation.presenters.impl.FetchMyWinsPresenterImpl;
import in.gadidekho.gadidekho.presentation.ui.adapters.MyWinsAdapter;
import in.gadidekho.gadidekho.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;

public class MyWinsActivity extends AppCompatActivity implements FetchMyWinsPresenter.View {

    @BindView(R.id.recycler_view_my_wins)
    RecyclerView recyclerViewMyWins;
    FetchMyWinsPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_wins);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchMyWins();
        showLoader();
        getSupportActionBar().setTitle("My Wins");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void initialisePresenter() {
        mPresenter = new FetchMyWinsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void loadAdapter(MyWinsAdapter adapter) {
        recyclerViewMyWins.setAdapter(adapter);
        recyclerViewMyWins.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
