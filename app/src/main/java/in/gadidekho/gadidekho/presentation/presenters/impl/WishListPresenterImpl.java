package in.gadidekho.gadidekho.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.AddToWishListInteractor;
import in.gadidekho.gadidekho.domain.interactors.FetchWishListInteractor;
import in.gadidekho.gadidekho.domain.interactors.RemoveFromWishlistInteractor;
import in.gadidekho.gadidekho.domain.interactors.impl.AddToWishListInteractorImpl;
import in.gadidekho.gadidekho.domain.interactors.impl.FetchWishListInteractorImpl;
import in.gadidekho.gadidekho.domain.interactors.impl.RemoveFromWishlistInteractorImpl;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.UserInfo;
import in.gadidekho.gadidekho.presentation.presenters.WishListPresenter;
import in.gadidekho.gadidekho.presentation.presenters.base.AbstractPresenter;
import in.gadidekho.gadidekho.presentation.ui.adapters.AuctionVehiclesAdapter;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class WishListPresenterImpl extends AbstractPresenter implements WishListPresenter,
                                                                        FetchWishListInteractor.Callback,
                                                                        AuctionVehiclesAdapter.Callback,
                                                                        AddToWishListInteractor.Callback,
                                                                        RemoveFromWishlistInteractor.Callback
                                                                {

    Context mContext;
    WishListPresenter.View mView;
    FetchWishListInteractorImpl fetchWishListInteractor;
    AuctionVehiclesAdapter auctionVehiclesAdapter;
    AddToWishListInteractorImpl addToWishListInteractor;
    AndroidApplication androidApplication;
    RemoveFromWishlistInteractorImpl removeFromWishlistInteractor;
    int position;

    public WishListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void onGettingWishListSuccess(AuctionVehicle[] auctionVehicles) {
        mView.hideLoader();
        if (auctionVehicles.length == 0) {
            Toasty.warning(mContext, "No Items Found", Toast.LENGTH_SHORT, true).show();
            AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setWishLists(mContext, null);
        } else {
            for (int i = 0; i < auctionVehicles.length; i++) {
                auctionVehicles[i].isWishListPresent = true;
            }
            androidApplication = (AndroidApplication) mContext.getApplicationContext();
            androidApplication.setWishLists(mContext, auctionVehicles);
            auctionVehiclesAdapter = new AuctionVehiclesAdapter(mContext, auctionVehicles, this);
            mView.loadAdapter(auctionVehiclesAdapter);
        }
    }

    @Override
    public void onGettingWishListFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.hideLoader();
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setWishLists(mContext, null);
    }

    @Override
    public void fetchWishList() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchWishListInteractor = new FetchWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
        fetchWishListInteractor.execute();
    }

    @Override
    public void onAuctionVehicleClicked(int id, int auctionId) {
        mView.goToProductDetails(id, auctionId);
    }

    @Override
    public void addToWishList(int id, int position) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        this.position = position;
        addToWishListInteractor = new AddToWishListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken, id);
        addToWishListInteractor.execute();
    }

    @Override
    public void removeFromWishList(int id, int position) {
        this.position = position;
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        removeFromWishlistInteractor = new RemoveFromWishlistInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken, id);
        removeFromWishlistInteractor.execute();
    }

    @Override
    public void onAddToWishListSuccess() {
        Toasty.success(mContext, "Successfully added to wishlist", Toast.LENGTH_SHORT, true).show();
        auctionVehiclesAdapter.onAddingWishListSuccess(position);

    }

    @Override
    public void onAddToWishListFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onRemoveFromWishListSuccess() {
        Toasty.success(mContext, "Successfully removed from wishlist", Toast.LENGTH_SHORT, true).show();
        auctionVehiclesAdapter.onAddingWishListFail(position);
        fetchWishList();
    }

    @Override
    public void onRemoveFromWishListFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
