package in.gadidekho.gadidekho.presentation.presenters;

import in.gadidekho.gadidekho.domain.models.DepositLimit;
import in.gadidekho.gadidekho.presentation.ui.adapters.AuctionAdapter;

public interface MainPresenter {

    void fetchAuctions();
    void fetchWishlist();
    void fetchDepositLimit();

    interface View {
        void loadAdapter(AuctionAdapter adapter);
        void hideLoader();
        void showLoader();
        void goToAuctionDetails(int id);
        void loadDepositLimit(DepositLimit depositLimit);
    }

}
