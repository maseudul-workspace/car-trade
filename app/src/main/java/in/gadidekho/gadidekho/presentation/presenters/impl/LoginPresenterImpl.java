package in.gadidekho.gadidekho.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.CheckLoginInteractor;
import in.gadidekho.gadidekho.domain.interactors.impl.CheckLoginInteratorImpl;
import in.gadidekho.gadidekho.domain.models.UserInfo;
import in.gadidekho.gadidekho.presentation.presenters.LoginPresenter;
import in.gadidekho.gadidekho.presentation.presenters.base.AbstractPresenter;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class LoginPresenterImpl extends AbstractPresenter implements LoginPresenter, CheckLoginInteractor.Callback {

    Context mContext;
    LoginPresenter.View mView;
    CheckLoginInteratorImpl mInteractor;
    AndroidApplication androidApplication;

    public LoginPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkLogin(String phone, String password) {
        mInteractor = new CheckLoginInteratorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone, password);
        mInteractor.execute();
    }

    @Override
    public void onLoginSuccess(UserInfo userInfo) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        androidApplication.setUserInfo(mContext, userInfo);
        Toasty.success(mContext, "Login Successfull", Toast.LENGTH_SHORT, true).show();
        mView.hideLoader();
        mView.goToMainActivity();
    }

    @Override
    public void onLoginFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.hideLoader();
    }
}
