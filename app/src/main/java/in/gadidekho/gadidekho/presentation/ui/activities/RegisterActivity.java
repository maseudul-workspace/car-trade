package in.gadidekho.gadidekho.presentation.ui.activities;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.provider.MediaStore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.loader.content.CursorLoader;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.provider.Settings;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.executors.impl.ThreadExecutor;
import in.gadidekho.gadidekho.presentation.presenters.RegisterPresenter;
import in.gadidekho.gadidekho.presentation.presenters.impl.RegisterPresenterImpl;
import in.gadidekho.gadidekho.threading.MainThreadImpl;
import in.gadidekho.gadidekho.util.GlideHelper;

public class RegisterActivity extends AppCompatActivity implements RegisterPresenter.View {

    @BindView(R.id.img_address_proof)
    ImageView imgViewAddressProof;
    @BindView(R.id.img_id_proof)
    ImageView imgIdProof;
    String addressProofImageUrl = "";
    String idProofImageUrl = "";
    RegisterPresenterImpl mPresenter;
    @BindView(R.id.edit_address)
    EditText editTextAddress;
    @BindView(R.id.edit_email)
    EditText editTextEmail;
    @BindView(R.id.edit_name)
    EditText editTextName;
    @BindView(R.id.edit_password)
    EditText editTextPassword;
    @BindView(R.id.edit_phone_no)
    EditText editTextPhoneNo;
    @BindView(R.id.edit_text_repeat_password)
    EditText editTextRepeatPassword;
    @BindView(R.id.progress_layout)
    View progressLayout;
    String[] appPremisions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    int flag = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
        ButterKnife.bind(this);
        initialisePresenter();
    }

    public void initialisePresenter() {
        mPresenter = new RegisterPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        if (resultCode == Activity.RESULT_OK) {
            switch (requestCode) {
                case 1000:
                    //data.getData return the content URI for the selected Image
                    Uri selectedAddressProofImage = data.getData();
                    addressProofImageUrl = getRealPathFromURI(selectedAddressProofImage);
                    GlideHelper.setImageView(this, imgViewAddressProof, addressProofImageUrl);
                    break;
                case 1001:
                    //data.getData return the content URI for the selected Image
                    Uri selectedIdImage = data.getData();
                    idProofImageUrl = getRealPathFromURI(selectedIdImage);
                    GlideHelper.setImageView(this, imgIdProof, idProofImageUrl);
                    break;
            }
        }
    }

    private String getRealPathFromURI(Uri contentUri) {
        String[] proj = {MediaStore.Images.Media.DATA};
        CursorLoader loader = new CursorLoader(this, contentUri, proj, null, null, null);
        Cursor cursor = loader.loadInBackground();
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String result = cursor.getString(column_index);
        cursor.close();
        return result;
    }

    @OnClick(R.id.img_address_proof) void onAddressProofClicked() {
        flag = 0;
        if(checkAndRequestPermissions()){
            //Create an Intent with action as ACTION_PICK
            Intent intent=new Intent(Intent.ACTION_PICK);
            // Sets the type as image/*. This ensures only components of type image are selected
            intent.setType("image/*");
            //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
            String[] mimeTypes = {"image/jpeg", "image/png"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
            // Launching the Intent
            startActivityForResult(intent,1000);
        }

    }

    @OnClick(R.id.img_id_proof) void onIdProofClicked() {
        flag = 1;
        if(checkAndRequestPermissions()){
            //Create an Intent with action as ACTION_PICK
            Intent intent=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
            // Sets the type as image/*. This ensures only components of type image are selected
            intent.setType("image/*");
            //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
            String[] mimeTypes = {"image/jpeg", "image/png"};
            intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
            // Launching the Intent
            startActivityForResult(intent,1001);
        }
    }

    @OnClick(R.id.btn_register) void register() {
        if (editTextAddress.getText().toString().trim().isEmpty() ||
                editTextEmail.getText().toString().trim().isEmpty() ||
                editTextName.getText().toString().trim().isEmpty() ||
                editTextPassword.getText().toString().trim().isEmpty() ||
                editTextPhoneNo.getText().toString().trim().isEmpty() ||
                editTextRepeatPassword.getText().toString().trim().isEmpty() ||
                addressProofImageUrl.isEmpty() ||
                idProofImageUrl.isEmpty()
                ) {

            Toasty.warning(this, "Please fill all the fields", Toast.LENGTH_SHORT, true).show();

        } else if (!editTextPassword.getText().toString().equals(editTextRepeatPassword.getText().toString())) {
            Toasty.warning(this, "Password mismatch", Toast.LENGTH_SHORT, true).show();
        } else {
            mPresenter.register(
                    editTextName.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextPhoneNo.getText().toString(),
                    editTextPassword.getText().toString(),
                    editTextAddress.getText().toString(),
                    addressProofImageUrl,
                    idProofImageUrl
            );
            showLoader();
        }
    }

    @Override
    public void hideLoader() {
        progressLayout.setVisibility(View.GONE);
    }

    @Override
    public void clearData() {
        editTextPassword.setText("");
        editTextRepeatPassword.setText("");
        editTextPhoneNo.setText("");
        editTextName.setText("");
        editTextEmail.setText("");
        editTextAddress.setText("");
        idProofImageUrl = "";
        addressProofImageUrl = "";
        imgIdProof.setImageDrawable(getDrawable(R.drawable.image_icon));
        imgViewAddressProof.setImageDrawable(getDrawable(R.drawable.image_icon));
    }

    public void showLoader() {
        progressLayout.setVisibility(View.VISIBLE);
    }

    public boolean checkAndRequestPermissions(){
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String perm: appPremisions){
            if(ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        if(!listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if(requestCode == PERMISSIONS_REQUEST_CODE){
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i<grantResults.length; i++){
                if( grantResults[i] == PackageManager.PERMISSION_DENIED ){
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if(deniedCount == 0){
//               download file
                if (flag == 0) {
                    //Create an Intent with action as ACTION_PICK
                    Intent intent=new Intent(Intent.ACTION_PICK);
                    // Sets the type as image/*. This ensures only components of type image are selected
                    intent.setType("image/*");
                    //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
                    String[] mimeTypes = {"image/jpeg", "image/png"};
                    intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
                    // Launching the Intent
                    startActivityForResult(intent,1000);
                } else {
                    //Create an Intent with action as ACTION_PICK
                    Intent intent=new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                    // Sets the type as image/*. This ensures only components of type image are selected
                    intent.setType("image/*");
                    //We pass an extra array with the accepted mime types. This will ensure only components with these MIME types as targeted.
                    String[] mimeTypes = {"image/jpeg", "image/png"};
                    intent.putExtra(Intent.EXTRA_MIME_TYPES,mimeTypes);
                    // Launching the Intent
                }
            }

            else{
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()){
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if(ActivityCompat.shouldShowRequestPermissionRationale(this, permName)){
                        this.showAlertDialog("", "This app needs read and write storage permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    }
                    else {
                        this.showAlertDialog("", "You have denied some permissions. Allow all permissions to download file at [Setting] > [Permissions]",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required to download file", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }
                }
            }

        }
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

}
