package in.gadidekho.gadidekho.presentation.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import android.util.Log;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.executors.impl.ThreadExecutor;
import in.gadidekho.gadidekho.domain.models.DepositLimit;
import in.gadidekho.gadidekho.domain.models.UserInfo;
import in.gadidekho.gadidekho.presentation.presenters.MainPresenter;
import in.gadidekho.gadidekho.presentation.presenters.impl.MainPresenterImpl;
import in.gadidekho.gadidekho.presentation.ui.adapters.AuctionAdapter;
import in.gadidekho.gadidekho.threading.MainThreadImpl;

public class MainActivity extends BaseActivity implements MainPresenter.View {

    AndroidApplication androidApplication;
    @BindView(R.id.recycler_view_auctions)
    RecyclerView recyclerViewAuctions;
    MainPresenterImpl mPresenter;
    @BindView(R.id.txt_view_deposit)
    TextView txtViewDeposit;
    @BindView(R.id.txt_view_available_limit)
    TextView txtViewAvailableLimit;
    @BindView(R.id.txt_view_buying_limit)
    TextView txtViewBuyingLimit;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        if (checkLogin()) {
            mPresenter.fetchAuctions();
            mPresenter.fetchWishlist();
            mPresenter.fetchDepositLimit();
            showLoader();
        } else {
            goToLogin();
        }
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }
    public boolean checkLogin() {
        androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo == null) {
            return false;
        } else {
//            Log.e("LogMsg", "User Id: " + userInfo.userId);
//            Log.e("LogMsg", "API Key: " + userInfo.apiToken);
            return true;
        }
    }

    public void goToLogin() {
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void loadAdapter(AuctionAdapter adapter) {
        recyclerViewAuctions.setAdapter(adapter);
        recyclerViewAuctions.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewAuctions.setNestedScrollingEnabled(false);
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void goToAuctionDetails(int id) {
        Intent intent = new Intent(this, AuctionVehicleActivity.class);
        intent.putExtra("auctionId", id);
        startActivity(intent);
    }

    @Override
    public void loadDepositLimit(DepositLimit depositLimit) {
        txtViewBuyingLimit.setText("Rs. " + depositLimit.buying_limit);
        txtViewAvailableLimit.setText("Rs. " + depositLimit.available_limit);
        txtViewDeposit.setText("Rs. " + depositLimit.deposit);
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (checkLogin()) {
            mPresenter.fetchDepositLimit();
        }
    }

}
