
package in.gadidekho.gadidekho.presentation.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import butterknife.BindView;
import butterknife.ButterKnife;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.executors.impl.ThreadExecutor;
import in.gadidekho.gadidekho.presentation.presenters.FetchAuctionVehiclesPresenter;
import in.gadidekho.gadidekho.presentation.presenters.impl.FetchAuctionVehiclesPresenterImpl;
import in.gadidekho.gadidekho.presentation.ui.adapters.AuctionVehiclesAdapter;
import in.gadidekho.gadidekho.threading.MainThreadImpl;

public class AuctionVehicleActivity extends AppCompatActivity implements FetchAuctionVehiclesPresenter.View {

    @BindView(R.id.recycler_view_auction_vehicle)
    RecyclerView recyclerViewAuctionVehicle;
    FetchAuctionVehiclesPresenterImpl mPresenter;
    int auctionId;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_auction_vehicle);
        ButterKnife.bind(this);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Auction Vehicles");
        auctionId = getIntent().getIntExtra("auctionId", 0);
        initialisePresenter();
        setUpProgressDialog();
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void initialisePresenter() {
        mPresenter = new FetchAuctionVehiclesPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @Override
    public void loadAdapter(AuctionVehiclesAdapter adapter) {
        recyclerViewAuctionVehicle.setAdapter(adapter);
        recyclerViewAuctionVehicle.setLayoutManager(new LinearLayoutManager(this));
    }

    @Override
    public void goToVehicleDetails(int vehicleId) {
        Intent intent = new Intent(this, VehicleDetailsActivity.class);
        intent.putExtra("auctionId", auctionId);
        intent.putExtra("vehicleId", vehicleId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchVehicles(auctionId);
        showLoader();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}
