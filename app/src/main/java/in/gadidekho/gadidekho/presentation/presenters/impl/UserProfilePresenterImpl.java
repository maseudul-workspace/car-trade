package in.gadidekho.gadidekho.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchUserDetailsInteractor;
import in.gadidekho.gadidekho.domain.interactors.UpdateUserInteractor;
import in.gadidekho.gadidekho.domain.interactors.impl.FetchUserDetailsInteractorImpl;
import in.gadidekho.gadidekho.domain.interactors.impl.UpdateUserInteractorImpl;
import in.gadidekho.gadidekho.domain.models.UserDetails;
import in.gadidekho.gadidekho.domain.models.UserInfo;
import in.gadidekho.gadidekho.presentation.presenters.UserProfilePresenter;
import in.gadidekho.gadidekho.presentation.presenters.base.AbstractPresenter;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class UserProfilePresenterImpl extends AbstractPresenter implements UserProfilePresenter, FetchUserDetailsInteractor.Callback, UpdateUserInteractor.Callback {

    Context mContext;
    UserProfilePresenter.View mView;
    AndroidApplication androidApplication;
    FetchUserDetailsInteractorImpl fetchUserDetailsInteractor;
    UpdateUserInteractorImpl updateUserInteractor;

    public UserProfilePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchUserDetails() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo  = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            fetchUserDetailsInteractor = new FetchUserDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
            fetchUserDetailsInteractor.execute();
        }
    }

    @Override
    public void onUpdateUserSuccess() {
        Toasty.success(mContext, "Successfully updated", Toast.LENGTH_SHORT, true).show();
        mView.hideLoader();
    }

    @Override
    public void onUpdateUserFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.hideLoader();
    }

    @Override
    public void updateUser(String userName, String email, String address) {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo  = androidApplication.getUserInfo(mContext);
        updateUserInteractor = new UpdateUserInteractorImpl(mExecutor, mMainThread, mContext, this, new AppRepositoryImpl(), userInfo.userId, userInfo.apiToken, userName, email, address);
        updateUserInteractor.execute();
    }

    @Override
    public void onGettingUserDetailsSuccess(UserDetails userDetails) {
        mView.hideLoader();
        mView.loadData(userDetails);
    }

    @Override
    public void onGettingUserDetailsFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
