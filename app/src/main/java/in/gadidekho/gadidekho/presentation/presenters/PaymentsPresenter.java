package in.gadidekho.gadidekho.presentation.presenters;

import in.gadidekho.gadidekho.presentation.ui.adapters.PaymentsAdapter;

public interface PaymentsPresenter {
    void fetchPayments();
    interface View {
        void loadAdapter(PaymentsAdapter adapter);
        void showLoader();
        void hideLoader();
        void goToPaymentActivity(int paymentId, double amount);
    }
}
