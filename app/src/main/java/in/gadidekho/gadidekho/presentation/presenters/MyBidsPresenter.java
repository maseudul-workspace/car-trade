package in.gadidekho.gadidekho.presentation.presenters;

import in.gadidekho.gadidekho.presentation.ui.adapters.AuctionVehiclesAdapter;

public interface MyBidsPresenter {
    void fetchMyBids();
    interface View {
        void loadAdapter(AuctionVehiclesAdapter adapter);
        void showLoader();
        void hideLoader();
        void goToProductDetails(int vehicleId, int auctionId);
    }
}
