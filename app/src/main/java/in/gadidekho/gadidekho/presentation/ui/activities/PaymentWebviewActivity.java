package in.gadidekho.gadidekho.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.models.UserInfo;

import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;

public class PaymentWebviewActivity extends AppCompatActivity {

    @BindView(R.id.webview)
    WebView webView;
    @BindView(R.id.layout_progress)
    View layoutProgress;
    int paymentId;
    double amount;
    AndroidApplication androidApplication;
    UserInfo userInfo;
    String baseUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_webview);
        ButterKnife.bind(this);
        paymentId = getIntent().getIntExtra("paymentId", 0);
        amount = getIntent().getDoubleExtra("amount", 0);
        androidApplication = (AndroidApplication) getApplicationContext();
        userInfo = androidApplication.getUserInfo(this);
        baseUrl = "http://carauction.softzoned.com/pay/" + paymentId + "/" + userInfo.userId + "/" + amount;
        checkInternetConnectivity();

    }

    public void checkInternetConnectivity() {
        if (isNetworkConnected()) {
            setUpWebview();
        } else {
            layoutProgress.setVisibility(View.GONE);
            Toast.makeText(this, "Please check your internet connection", Toast.LENGTH_LONG).show();
        }
    }

    public void setUpWebview() {
        webView = findViewById(R.id.webview);

//        Webview settings
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setAllowContentAccess(true);
        webView.getSettings().setAllowFileAccess(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.setScrollBarStyle(WebView.SCROLLBARS_OUTSIDE_OVERLAY);
        webView.setScrollbarFadingEnabled(false);
        webView.getSettings().setSupportZoom(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setUserAgentString(webView.getSettings().getUserAgentString().replace("; wv",""));
        webView.getSettings().setJavaScriptCanOpenWindowsAutomatically(true);
        webView.getSettings().setSupportMultipleWindows(true);

//        Load url in webview
        webView.loadUrl(baseUrl);

        webView.setWebViewClient(new WebViewClient(){

            // Show loader on url load
            public void onLoadResource (WebView view, String url) {

                // if url contains string androidexample
                // Then show progress  Dialog
                // in standard case YourActivity.this

            }

            // Called when all page resources loaded
            public void onPageFinished(WebView view, String url) {

                try {
                    // Close progressDialog
                    layoutProgress.setVisibility(View.GONE);

                    webView.setVisibility(View.VISIBLE);

                } catch (Exception exception) {
                    layoutProgress.setVisibility(View.GONE);
                    Toast.makeText(getApplicationContext(), "Something went wrong", Toast.LENGTH_SHORT).show();
                    exception.printStackTrace();
                }
            }

            @Override
            public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
                super.onReceivedError(view, request, error);
                view.loadUrl("about:blank");
            }
        });
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

}
