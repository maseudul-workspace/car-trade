package in.gadidekho.gadidekho.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.models.UpcomingAuction;

public class UpcomingAuctionAdapter extends RecyclerView.Adapter<UpcomingAuctionAdapter.ViewHolder> {

    Context mContext;
    UpcomingAuction[] upcomingAuctions;

    public UpcomingAuctionAdapter(Context mContext, UpcomingAuction[] upcomingAuctions) {
        this.mContext = mContext;
        this.upcomingAuctions = upcomingAuctions;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_upcoming_auctions, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewAuction.setText(upcomingAuctions[position].auctionGroupName);
        holder.txtViewStartDate.setText(upcomingAuctions[position].auctionStartDate);
        holder.txtViewEndDate.setText(upcomingAuctions[position].auctionEndDate);
        holder.txtViewTotalVehicle.setText(Integer.toString(upcomingAuctions[position].total_vehicle));
    }

    @Override
    public int getItemCount() {
        return upcomingAuctions.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_auction_title)
        TextView txtViewAuction;
        @BindView(R.id.txt_view_total_vehicle)
        TextView txtViewTotalVehicle;
        @BindView(R.id.txt_view_start_date)
        TextView txtViewStartDate;
        @BindView(R.id.txt_view_end_date)
        TextView txtViewEndDate;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
