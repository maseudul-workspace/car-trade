package in.gadidekho.gadidekho.presentation.presenters.impl;

import android.Manifest;
import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.RegisterInteractor;
import in.gadidekho.gadidekho.domain.interactors.impl.RegisterInteractorImpl;
import in.gadidekho.gadidekho.presentation.presenters.RegisterPresenter;
import in.gadidekho.gadidekho.presentation.presenters.base.AbstractPresenter;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class RegisterPresenterImpl extends AbstractPresenter implements RegisterPresenter, RegisterInteractor.Callback {

    Context mContext;
    RegisterPresenter.View mView;
    RegisterInteractorImpl mInteractor;
    String[] appPremisions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;

    public RegisterPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void register(String name, String email, String mobile, String password, String address, String addressProofFile, String idProofFile) {
        mInteractor = new RegisterInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), name, email, mobile, password, address, addressProofFile, idProofFile);
        mInteractor.execute();
    }

    @Override
    public void onRegisterSuccess() {
        mView.hideLoader();
        mView.clearData();
        Toasty.success(mContext, "Successfully registered!! PLease wait for verification", Toast.LENGTH_LONG, true).show();
    }

    @Override
    public void onRegsiterFail() {
        mView.hideLoader();
        Toasty.error(mContext, "Failed to register", Toast.LENGTH_SHORT, true).show();
    }
}
