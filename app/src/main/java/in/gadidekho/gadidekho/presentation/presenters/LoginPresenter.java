package in.gadidekho.gadidekho.presentation.presenters;

public interface LoginPresenter {
    void checkLogin(String phone, String password);
    interface View {
        void goToMainActivity();
        void showLoader();
        void hideLoader();
    }
}
