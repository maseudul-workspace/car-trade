package in.gadidekho.gadidekho.presentation.presenters;

import in.gadidekho.gadidekho.domain.models.VehicleDetails;

public interface FetchVehicleDetailsPresenter {
    void fetchVehicleDetails(int auctionId, int vehicleId);
    void submitBid(int vehicleId, int bid);
    void addToWishlist();
    void removeFromWishlist();
    interface View {
        void loadData(VehicleDetails vehicleDetails);
        void showLoader();
        void hideLoader();
        void onWishListAddingSuccess();
        void onWishListRemovingSuccess();
        void stopRefreshing();
    }
}
