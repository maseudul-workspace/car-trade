package in.gadidekho.gadidekho.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.executors.impl.ThreadExecutor;
import in.gadidekho.gadidekho.presentation.presenters.CheckOTPPresenter;
import in.gadidekho.gadidekho.presentation.presenters.impl.CheckOTPPresenterImpl;
import in.gadidekho.gadidekho.threading.MainThreadImpl;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class CheckOTPActivity extends AppCompatActivity implements CheckOTPPresenter.View {

    @BindView(R.id.edit_text_mobile)
    EditText editTextMobile;
    @BindView(R.id.edit_text_otp)
    EditText editTextOTP;
    @BindView(R.id.btn_check_otp)
    Button btnCheckOTP;
    @BindView(R.id.btn_send_otp)
    Button btnSendOTP;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    CheckOTPPresenterImpl checkOTPPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_check_otp);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
    }

    public void initialisePresenter() {
        checkOTPPresenter = new CheckOTPPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_send_otp) void onSendOTPClicked() {
        if (editTextMobile.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "Please enter mobile number", Toast.LENGTH_SHORT, true).show();
        } else {
            checkOTPPresenter.checkOTP(editTextMobile.getText().toString());
            showLoader();
        }
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @Override
    public void onCheckOTPSuccess() {
        editTextOTP.setVisibility(View.VISIBLE);
        editTextPassword.setVisibility(View.VISIBLE);
        btnSendOTP.setVisibility(View.GONE);
        btnCheckOTP.setVisibility(View.VISIBLE);
    }

    @Override
    public void onCheckOTPFail() {

    }

    @Override
    public void onPasswordResettingSuccess() {
        finish();
    }

    @OnClick(R.id.btn_check_otp) void onCheckOTPClicked() {
        if (editTextMobile.getText().toString().trim().isEmpty() || editTextPassword.getText().toString().isEmpty() || editTextOTP.getText().toString().isEmpty()) {
           Toasty.warning(this, "Please fill all the fields", Toast.LENGTH_SHORT, true).show();
        } else {
            checkOTPPresenter.resetPassword(editTextMobile.getText().toString(), editTextOTP.getText().toString(), editTextPassword.getText().toString());
            showLoader();
        }
    }

}
