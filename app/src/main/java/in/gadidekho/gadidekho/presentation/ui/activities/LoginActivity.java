package in.gadidekho.gadidekho.presentation.ui.activities;

import android.app.ProgressDialog;
import android.content.Intent;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.EditText;
import android.widget.Toast;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.executors.impl.ThreadExecutor;
import in.gadidekho.gadidekho.presentation.presenters.LoginPresenter;
import in.gadidekho.gadidekho.presentation.presenters.impl.LoginPresenterImpl;
import in.gadidekho.gadidekho.threading.MainThreadImpl;

public class LoginActivity extends AppCompatActivity implements LoginPresenter.View {

    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    LoginPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        initialisePresenter();
        ButterKnife.bind(this);
        setUpProgressDialog();
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    public void initialisePresenter() {
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    @OnClick(R.id.btn_login) void onCheckLoginClicked() {
        if (editTextPhone.getText().toString().trim().isEmpty() || editTextPassword.getText().toString().trim().isEmpty()) {
            Toasty.warning(this, "All fields requires", Toast.LENGTH_SHORT, true).show();
        } else {
            mPresenter.checkLogin(editTextPhone.getText().toString(), editTextPassword.getText().toString());
            showLoader();
        }
    }

    @Override
    public void goToMainActivity() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.hide();
    }

    @OnClick(R.id.txt_view_register) void onRegisterClicked() {
        Intent registerIntent = new Intent(this, RegisterActivity.class);
        startActivity(registerIntent);
    }

    @OnClick(R.id.txt_view_forget_password) void onForgetPasswordClicked() {
        Intent intent = new Intent(this, CheckOTPActivity.class);
        startActivity(intent);
    }

}
