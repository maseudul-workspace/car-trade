package in.gadidekho.gadidekho.presentation.ui.adapters;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.models.Notification;

public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {

    public interface Callback {
        void onNotificationClicked();
    }

    Context mContext;
    Notification[] notifications;
    Callback mCallback;

    public NotificationsAdapter(Context mContext, Notification[] notifications, Callback mCallback) {
        this.mContext = mContext;
        this.notifications = notifications;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_notification, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewNotificationTitle.setText(notifications[position].title);
        holder.txtViewNotificationDescription.setText(Html.fromHtml(notifications[position].desc));
    }

    @Override
    public int getItemCount() {
        return notifications.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_notification_title)
        TextView txtViewNotificationTitle;
        @BindView(R.id.txt_view_notification_desc)
        TextView txtViewNotificationDescription;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
