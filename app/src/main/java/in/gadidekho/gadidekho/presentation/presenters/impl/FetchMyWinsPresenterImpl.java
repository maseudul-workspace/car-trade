package in.gadidekho.gadidekho.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchMyWinsInteractor;
import in.gadidekho.gadidekho.domain.interactors.impl.FetchMyWinsInteractorImpl;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.UserInfo;
import in.gadidekho.gadidekho.presentation.presenters.FetchMyWinsPresenter;
import in.gadidekho.gadidekho.presentation.presenters.base.AbstractPresenter;
import in.gadidekho.gadidekho.presentation.ui.adapters.MyWinsAdapter;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class FetchMyWinsPresenterImpl extends AbstractPresenter implements FetchMyWinsPresenter, FetchMyWinsInteractor.Callback {

    Context mContext;
    FetchMyWinsPresenter.View mView;
    AndroidApplication androidApplication;
    FetchMyWinsInteractorImpl fetchMyWinsInteractor;

    public FetchMyWinsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchMyWins() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        fetchMyWinsInteractor = new FetchMyWinsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.userId, userInfo.apiToken);
        fetchMyWinsInteractor.execute();
    }

    @Override
    public void onGettingMyWinsSuccess(AuctionVehicle[] auctionVehicles) {
        MyWinsAdapter myWinsAdapter = new MyWinsAdapter(mContext, auctionVehicles);
        mView.hideLoader();
        mView.loadAdapter(myWinsAdapter);
    }

    @Override
    public void onGettingMyWinsFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
