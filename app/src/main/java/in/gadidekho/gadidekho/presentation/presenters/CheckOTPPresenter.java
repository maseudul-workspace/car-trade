package in.gadidekho.gadidekho.presentation.presenters;

public interface CheckOTPPresenter {
    void checkOTP(String phone);
    void resetPassword(String phone, String confirmCode, String password);
    interface View {
        void showLoader();
        void hideLoader();
        void onCheckOTPSuccess();
        void onCheckOTPFail();
        void onPasswordResettingSuccess();
    }
}
