package in.gadidekho.gadidekho.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.ChangePasswordInteractor;
import in.gadidekho.gadidekho.domain.interactors.CheckOTPInteractor;
import in.gadidekho.gadidekho.domain.interactors.impl.ChangePasswordInteractorImpl;
import in.gadidekho.gadidekho.domain.interactors.impl.CheckOTPInteractorImpl;
import in.gadidekho.gadidekho.presentation.presenters.CheckOTPPresenter;
import in.gadidekho.gadidekho.presentation.presenters.base.AbstractPresenter;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class CheckOTPPresenterImpl extends AbstractPresenter implements CheckOTPPresenter, CheckOTPInteractor.Callback, ChangePasswordInteractor.Callback {

    Context mContext;
    CheckOTPPresenter.View mView;
    CheckOTPInteractorImpl checkOTPInteractor;
    ChangePasswordInteractorImpl changePasswordInteractor;

    public CheckOTPPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void checkOTP(String phone) {
        checkOTPInteractor = new CheckOTPInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone);
        checkOTPInteractor.execute();
    }

    @Override
    public void resetPassword(String phone, String confirmCode, String password) {
        changePasswordInteractor = new ChangePasswordInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone, confirmCode, password);
        changePasswordInteractor.execute();
    }

    @Override
    public void onChangePasswordSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Password Changed Successfully", Toast.LENGTH_SHORT, true).show();
        mView.onPasswordResettingSuccess();
    }

    @Override
    public void onChangePasswordFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }

    @Override
    public void onCheckOTPSucces() {
        mView.hideLoader();
        Toasty.success(mContext, "OTP Sent", Toast.LENGTH_SHORT, true).show();
        mView.onCheckOTPSuccess();
    }

    @Override
    public void onCheckOTPFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
    }
}
