package in.gadidekho.gadidekho.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import in.gadidekho.gadidekho.R;
import in.gadidekho.gadidekho.domain.models.Payments;

public class PaymentsAdapter extends RecyclerView.Adapter<PaymentsAdapter.ViewHolder> {

    public interface Callback {
        void onPaymentClicked(int paymentId, double amount);
    }

    Context mContext;
    Payments[] payments;
    Callback mCallback;

    public PaymentsAdapter(Context mContext, Payments[] payments, Callback mCallback) {
        this.mContext = mContext;
        this.payments = payments;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_payments, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewMessage.setText(payments[position].message);
        holder.txtViewPaymentAmount.setText("Rs. " + Double.toString(payments[position].amount));
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onPaymentClicked(payments[position].id, payments[position].amount);
            }
        });
    }

    @Override
    public int getItemCount() {
        return payments.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_payment_amount)
        TextView txtViewPaymentAmount;
        @BindView(R.id.txt_view_message)
        TextView txtViewMessage;
        @BindView(R.id.layout_main)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
