package in.gadidekho.gadidekho.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import es.dmoral.toasty.Toasty;
import in.gadidekho.gadidekho.AndroidApplication;
import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchNotificationsInteractor;
import in.gadidekho.gadidekho.domain.interactors.impl.FetchNotificationsInteractorImpl;
import in.gadidekho.gadidekho.domain.models.Notification;
import in.gadidekho.gadidekho.domain.models.UserInfo;
import in.gadidekho.gadidekho.presentation.presenters.NotificationsPresenter;
import in.gadidekho.gadidekho.presentation.presenters.base.AbstractPresenter;
import in.gadidekho.gadidekho.presentation.ui.adapters.NotificationsAdapter;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class NotificationsPresenterImpl extends AbstractPresenter implements NotificationsPresenter, FetchNotificationsInteractor.Callback, NotificationsAdapter.Callback {

    Context mContext;
    NotificationsPresenter.View mView;
    FetchNotificationsInteractorImpl mInteractor;
    AndroidApplication androidApplication;
    NotificationsAdapter notificationsAdapter;

    public NotificationsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchNotifications() {
        androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        mInteractor = new FetchNotificationsInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.userId, userInfo.apiToken);
        mInteractor.execute();
    }

    @Override
    public void onGettingNotificationsSuccess(Notification[] notifications) {
        mView.hideLoader();
        if (notifications.length == 0) {
            Toasty.warning(mContext, "No Notifications Found", Toast.LENGTH_SHORT, true).show();
        } else {
            notificationsAdapter = new NotificationsAdapter(mContext, notifications, this::fetchNotifications);
            mView.loadAdapter(notificationsAdapter);
        }
    }

    @Override
    public void onGettingNotificationFail(String errorMsg) {
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT, true).show();
        mView.hideLoader();
    }

    @Override
    public void onNotificationClicked() {

    }
}
