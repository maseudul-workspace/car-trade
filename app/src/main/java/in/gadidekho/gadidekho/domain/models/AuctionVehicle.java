package in.gadidekho.gadidekho.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AuctionVehicle {

    @SerializedName("time")
    @Expose
    public String time;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("vehicle_name")
    @Expose
    public String vehicleName;

    @SerializedName("vehicle_id")
    @Expose
    public int vehicleId;

    @SerializedName("auction_id")
    @Expose
    public int auctionId;

    @SerializedName("images")
    @Expose
    public Images[] images;

    @SerializedName("regisation_no")
    @Expose
    public String registrationNo;

    @SerializedName("regisation_available")
    @Expose
    public String registrationAvailable;

    @SerializedName("mfg_month_year")
    @Expose
    public String mfg_month_year;

    @SerializedName("fuel_type")
    @Expose
    public String fuel_type;

    @SerializedName("owner_type")
    @Expose
    public String owner_type;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("transmission_type")
    @Expose
    public String transmission_type;

    @SerializedName("total_remaining_bids")
    @Expose
    public int total_remaining_bids;

    @SerializedName("current_bid_amount")
    @Expose
    public int current_bid_amount;

    public boolean isWishListPresent = false;

}
