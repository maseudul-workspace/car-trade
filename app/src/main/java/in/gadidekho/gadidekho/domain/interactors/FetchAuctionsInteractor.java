package in.gadidekho.gadidekho.domain.interactors;

import in.gadidekho.gadidekho.domain.models.Auction;

public interface FetchAuctionsInteractor {
    interface Callback {
        void onFetchAuctionSuccess(Auction[] auctions);
        void onFetchAuctionFail(String errorMsg);
    }
}
