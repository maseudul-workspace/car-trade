package in.gadidekho.gadidekho.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Payments {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("msg")
    @Expose
    public String message;

    @SerializedName("amount")
    @Expose
    public double amount;

}
