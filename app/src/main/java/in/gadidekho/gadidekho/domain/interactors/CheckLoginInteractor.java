package in.gadidekho.gadidekho.domain.interactors;

import in.gadidekho.gadidekho.domain.models.UserInfo;

public interface CheckLoginInteractor {
    interface Callback {
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
