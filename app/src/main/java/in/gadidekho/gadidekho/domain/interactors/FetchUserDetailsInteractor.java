package in.gadidekho.gadidekho.domain.interactors;

import in.gadidekho.gadidekho.domain.models.UserDetails;

public interface FetchUserDetailsInteractor {
    interface Callback {
        void onGettingUserDetailsSuccess(UserDetails userDetails);
        void onGettingUserDetailsFail(String errorMsg);
    }
}
