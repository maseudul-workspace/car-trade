package in.gadidekho.gadidekho.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class NotificationsWrapper {
    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("message")
    @Expose
    public String message;

    @SerializedName("data")
    @Expose
    public Notification[] notifications;
}
