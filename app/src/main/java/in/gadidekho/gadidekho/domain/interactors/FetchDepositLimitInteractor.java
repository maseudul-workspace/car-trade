package in.gadidekho.gadidekho.domain.interactors;

import in.gadidekho.gadidekho.domain.models.DepositLimit;

public interface FetchDepositLimitInteractor {
    interface Callback {
        void onGettingDepositLimitSuccess(DepositLimit depositLimit);
        void onGettingDepositLimitFail(String errorMsg);
    }
}
