package in.gadidekho.gadidekho.domain.interactors;

public interface UpdateUserInteractor {
    interface Callback {
        void onUpdateUserSuccess();
        void onUpdateUserFail(String errorMsg);
    }
}
