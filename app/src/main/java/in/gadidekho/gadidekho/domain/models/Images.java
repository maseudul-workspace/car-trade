package in.gadidekho.gadidekho.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Images {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("img")
    @Expose
    public String img;

}
