package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchAuctionsInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.Auction;
import in.gadidekho.gadidekho.domain.models.AuctionWrapper;
import in.gadidekho.gadidekho.domain.models.UserInfo;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class FetchAuctionInteractorImpl extends AbstractInteractor implements FetchAuctionsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiKey;

    public FetchAuctionInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiKey) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiKey = apiKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchAuctionFail(errorMsg);
            }
        });
    }

    private void postMessage(final Auction[] auctions){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchAuctionSuccess(auctions);
            }
        });
    }

    @Override
    public void run() {
        final AuctionWrapper auctionWrapper = mRepository.getAuctions(userId, apiKey);
        if (auctionWrapper == null) {
            notifyError("Something went wrong");
        } else if (!auctionWrapper.status) {
            notifyError(auctionWrapper.message);
        } else {
            postMessage(auctionWrapper.auctions);
        }
    }
}
