package in.gadidekho.gadidekho.domain.interactors;

import in.gadidekho.gadidekho.domain.models.AuctionVehicle;

public interface FetchMyWinsInteractor {
    interface Callback {
        void onGettingMyWinsSuccess(AuctionVehicle[] auctionVehicles);
        void onGettingMyWinsFail(String errorMsg);
    }
}
