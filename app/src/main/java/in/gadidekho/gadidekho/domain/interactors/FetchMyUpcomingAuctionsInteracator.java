package in.gadidekho.gadidekho.domain.interactors;

import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.UpcomingAuction;

public interface FetchMyUpcomingAuctionsInteracator {
    interface Callback {
        void onFetchingMyUpcomingAuctionsSuccess(UpcomingAuction[] upcomingAuctions);
        void onFetchingMyUpcomingAuctionsFail(String errorMsg);
    }
}
