package in.gadidekho.gadidekho.domain.interactors;

import in.gadidekho.gadidekho.domain.models.AuctionVehicle;

public interface FetchWishListInteractor {
    interface Callback {
        void onGettingWishListSuccess(AuctionVehicle[] auctionVehicles);
        void onGettingWishListFail(String errorMsg);
    }
}
