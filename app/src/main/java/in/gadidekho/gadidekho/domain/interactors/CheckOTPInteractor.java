package in.gadidekho.gadidekho.domain.interactors;

public interface CheckOTPInteractor {
    interface Callback {
        void onCheckOTPSucces();
        void onCheckOTPFail(String errorMsg);
    }
}
