package in.gadidekho.gadidekho.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DepositLimit {
    @SerializedName("deposit")
    @Expose
    public String deposit;

    @SerializedName("buying_limit")
    @Expose
    public String buying_limit;

    @SerializedName("available_limit")
    @Expose
    public String available_limit;
}
