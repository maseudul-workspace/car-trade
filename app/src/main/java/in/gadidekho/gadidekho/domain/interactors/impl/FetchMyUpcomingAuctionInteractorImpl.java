package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchMyUpcomingAuctionsInteracator;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.AuctionVehicleWrapper;
import in.gadidekho.gadidekho.domain.models.UpcomingAuction;
import in.gadidekho.gadidekho.domain.models.UpcomingAuctionWrapper;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class FetchMyUpcomingAuctionInteractorImpl extends AbstractInteractor implements FetchMyUpcomingAuctionsInteracator {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int userId;
    String apiKey;

    public FetchMyUpcomingAuctionInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int userId, String apiKey) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.userId = userId;
        this.apiKey = apiKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingMyUpcomingAuctionsFail(errorMsg);
            }
        });
    }

    private void postMessage(final UpcomingAuction[] upcomingAuctions){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchingMyUpcomingAuctionsSuccess(upcomingAuctions);
            }
        });
    }

    @Override
    public void run() {
        final UpcomingAuctionWrapper upcomingAuctionWrapper = mRepository.fetchUpcomings(userId, apiKey);
        if (upcomingAuctionWrapper == null) {
            notifyError("Something went wrong");
        } else if (!upcomingAuctionWrapper.status) {
            notifyError(upcomingAuctionWrapper.message);
        } else {
            postMessage(upcomingAuctionWrapper.upcomingAuctions);
        }
    }
}
