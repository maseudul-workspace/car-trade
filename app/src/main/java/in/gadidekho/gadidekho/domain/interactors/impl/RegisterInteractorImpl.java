package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.RegisterInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.SuccessResponse;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class RegisterInteractorImpl extends AbstractInteractor implements RegisterInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String name;
    String email;
    String mobile;
    String password;
    String address;
    String addressProofFile;
    String idProofFile;

    public RegisterInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, String name, String email, String mobile, String password, String address, String addressProofFile, String idProofFile) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.name = name;
        this.email = email;
        this.mobile = mobile;
        this.password = password;
        this.address = address;
        this.addressProofFile = addressProofFile;
        this.idProofFile = idProofFile;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegsiterFail();
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterSuccess();
            }
        });
    }


    @Override
    public void run() {
        final SuccessResponse successResponse = mRepository.registerUser(name, email, mobile, password, address, addressProofFile, idProofFile);
        if (successResponse == null) {
            notifyError();
        } else if (!successResponse.status) {
            notifyError();
        } else {
            postMessage();
        }
    }
}
