package in.gadidekho.gadidekho.domain.interactors;

import in.gadidekho.gadidekho.domain.models.Notification;

public interface FetchNotificationsInteractor {
    interface Callback {
        void onGettingNotificationsSuccess(Notification[] notifications);
        void onGettingNotificationFail(String errorMsg);
    }
}
