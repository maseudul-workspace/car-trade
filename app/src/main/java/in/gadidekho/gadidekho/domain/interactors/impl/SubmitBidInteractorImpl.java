package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.SubmitBidInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.SuccessResponse;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class SubmitBidInteractorImpl extends AbstractInteractor implements SubmitBidInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;
    int vehicleId;
    int bidAmount;

    public SubmitBidInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken, int vehicleId, int bidAmount) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
        this.vehicleId = vehicleId;
        this.bidAmount = bidAmount;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBiddingFail();
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onBiddingSuccess();
            }
        });
    }


    @Override
    public void run() {
        final SuccessResponse successResponse = mRepository.submitBid(userId, apiToken, vehicleId, bidAmount);

        if (successResponse == null) {
            notifyError();
        } else if (!successResponse.status) {
            notifyError();
        } else {
            postMessage();
        }

    }
}
