package in.gadidekho.gadidekho.domain.interactors;

import in.gadidekho.gadidekho.domain.models.VehicleDetails;

public interface FetchVehicleDetailsInteractor {
    interface Callback {
        void onGettingVehicleDetailsSuccess(VehicleDetails vehicleDetails);
        void onGettingVehicleDetailsFail(String errorMsg);
    }
}
