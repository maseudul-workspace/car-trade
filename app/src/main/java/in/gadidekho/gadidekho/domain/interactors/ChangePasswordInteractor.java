package in.gadidekho.gadidekho.domain.interactors;

public interface ChangePasswordInteractor {
    interface Callback {
        void onChangePasswordSuccess();
        void onChangePasswordFail(String errorMsg);
    }
}
