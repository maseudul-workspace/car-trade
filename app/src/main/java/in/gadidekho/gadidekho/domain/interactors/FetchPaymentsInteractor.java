package in.gadidekho.gadidekho.domain.interactors;

import in.gadidekho.gadidekho.domain.models.Payments;

public interface FetchPaymentsInteractor {
    interface Callback {
        void onPaymentFetchSuccess(Payments[] payments);
        void onPaymentFetchFailed(String errorMsg);
    }
}
