package in.gadidekho.gadidekho.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserInfoWrapper {
    @SerializedName("code")
    @Expose
    public int code;

    @SerializedName("status")
    @Expose
    public boolean status;

    @SerializedName("data")
    @Expose
    public UserInfo userInfo;

    @SerializedName("message")
    @Expose
    public String message;

}
