package in.gadidekho.gadidekho.domain.interactors;

public interface RemoveFromWishlistInteractor {
    interface Callback {
        void onRemoveFromWishListSuccess();
        void onRemoveFromWishListFail(String errorMsg);
    }
}
