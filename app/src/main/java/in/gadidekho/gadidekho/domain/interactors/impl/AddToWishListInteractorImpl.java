package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.AddToWishListInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.SuccessResponse;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class AddToWishListInteractorImpl extends AbstractInteractor implements AddToWishListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;
    int vehicleId;

    public AddToWishListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken, int vehicleId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
        this.vehicleId = vehicleId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToWishListFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onAddToWishListSuccess();
            }
        });
    }

    @Override
    public void run() {
        final SuccessResponse successResponse = mRepository.addToWishList(userId, apiToken, vehicleId);
        if (successResponse == null) {
            notifyError("Something went wrong");
        } else if (!successResponse.status) {
            notifyError(successResponse.message);
        } else {
            postMessage();
        }
    }
}
