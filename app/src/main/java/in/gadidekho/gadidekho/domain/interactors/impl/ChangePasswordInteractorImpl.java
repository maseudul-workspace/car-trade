package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.ChangePasswordInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.SuccessResponse;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class ChangePasswordInteractorImpl extends AbstractInteractor implements ChangePasswordInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String mobileNo;
    String code;
    String password;

    public ChangePasswordInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String mobileNo, String code, String password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.mobileNo = mobileNo;
        this.code = code;
        this.password = password;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangePasswordFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onChangePasswordSuccess();
            }
        });
    }

    @Override
    public void run() {
        final SuccessResponse successResponse = mRepository.changePassword(mobileNo, code, password);
        if (successResponse ==  null) {
           notifyError("Something went wrong");
        } else if (!successResponse.status) {
            notifyError(successResponse.message);
        } else {
            postMessage();
        }
    }
}
