package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.RemoveFromWishlistInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.SuccessResponse;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class RemoveFromWishlistInteractorImpl extends AbstractInteractor implements RemoveFromWishlistInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;
    int vehicleId;

    public RemoveFromWishlistInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken, int vehicleId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
        this.vehicleId = vehicleId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRemoveFromWishListFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRemoveFromWishListSuccess();
            }
        });
    }

    @Override
    public void run() {
        SuccessResponse successResponse = mRepository.removeFromWishList(userId, apiToken, vehicleId);
        if (successResponse == null) {
           notifyError("Something went wrong");
        } else if (!successResponse.status) {
            notifyError(successResponse.message);
        } else {
            postMessage();
        }
    }
}
