package in.gadidekho.gadidekho.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Auction {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("auction_group_name")
    @Expose
    public String auctionGroupName;

    @SerializedName("total_vehicle")
    @Expose
    public String totalVehicle;

    @SerializedName("time")
    @Expose
    public String time;

    @SerializedName("auction_end_date")
    @Expose
    public String auction_end_date;

}
