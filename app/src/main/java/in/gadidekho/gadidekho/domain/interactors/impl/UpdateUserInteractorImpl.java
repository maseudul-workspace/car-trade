package in.gadidekho.gadidekho.domain.interactors.impl;

import android.content.Context;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.UpdateUserInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.SuccessResponse;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class UpdateUserInteractorImpl extends AbstractInteractor implements UpdateUserInteractor {

    Context mContext;
    Callback mCallback;
    AppRepositoryImpl mRepository;
    int userId;
    String apiToken;
    String userName;
    String email;
    String address;

    public UpdateUserInteractorImpl(Executor threadExecutor, MainThread mainThread, Context mContext, Callback mCallback, AppRepositoryImpl repository, int userId, String apiToken, String userName, String email, String address) {
        super(threadExecutor, mainThread);
        this.mContext = mContext;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
        this.userName = userName;
        this.email = email;
        this.address = address;
        mRepository = repository;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateUserFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onUpdateUserSuccess();
            }
        });
    }

    @Override
    public void run() {
        final SuccessResponse successResponse = mRepository.updateUser(userId, apiToken, userName, email, address);
        if (successResponse == null) {
            notifyError("Something went wrong");
        } else if (!successResponse.status) {
            notifyError(successResponse.message);
        } else {
            postMessage();
        }
    }
}
