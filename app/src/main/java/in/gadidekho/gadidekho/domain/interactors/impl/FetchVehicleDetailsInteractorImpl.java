package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchVehicleDetailsInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.VehicleDetails;
import in.gadidekho.gadidekho.domain.models.VehicleDetailsWrapper;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class FetchVehicleDetailsInteractorImpl extends AbstractInteractor implements FetchVehicleDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;
    int auctionId;
    int vehicleId;

    public FetchVehicleDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken, int auctionId, int vehicleId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
        this.auctionId = auctionId;
        this.vehicleId = vehicleId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingVehicleDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(final VehicleDetails vehicleDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingVehicleDetailsSuccess(vehicleDetails);
            }
        });
    }

    @Override
    public void run() {
        final VehicleDetailsWrapper vehicleDetailsWrapper = mRepository.fetchVeicleDetails(userId, apiToken, auctionId, vehicleId);
        if (vehicleDetailsWrapper == null) {
            notifyError("Something went wrong");
        } else if (!vehicleDetailsWrapper.status) {
            notifyError(vehicleDetailsWrapper.message);
        } else {
            postMessage(vehicleDetailsWrapper.vehicleDetails);
        }
    }
}
