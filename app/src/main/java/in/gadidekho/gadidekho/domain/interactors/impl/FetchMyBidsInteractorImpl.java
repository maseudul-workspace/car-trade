package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchMyBidsInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.AuctionVehicleWrapper;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class FetchMyBidsInteractorImpl extends AbstractInteractor implements FetchMyBidsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiKey;

    public FetchMyBidsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiKey) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiKey = apiKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMyBidsFail(errorMsg);
            }
        });
    }

    private void postMessage(final AuctionVehicle[] auctionVehicles){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMyBidsSuccess(auctionVehicles);
            }
        });
    }

    @Override
    public void run() {
        final AuctionVehicleWrapper auctionVehicleWrapper = mRepository.fetchMyBids(userId, apiKey);
        if (auctionVehicleWrapper ==  null) {
            notifyError("Something went wrong");
        } else if (!auctionVehicleWrapper.status) {
            notifyError(auctionVehicleWrapper.message);
        } else {
            postMessage(auctionVehicleWrapper.auctionVehicles);
        }
    }
}
