package in.gadidekho.gadidekho.domain.interactors;

public interface AddToWishListInteractor {
    interface Callback {
        void onAddToWishListSuccess();
        void onAddToWishListFail(String errorMsg);
    }
}
