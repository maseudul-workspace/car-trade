package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchPaymentsInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.Payments;
import in.gadidekho.gadidekho.domain.models.PaymentsWrapper;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class FetchPaymentsInteractorImpl extends AbstractInteractor implements FetchPaymentsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;

    public FetchPaymentsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPaymentFetchFailed(errorMsg);
            }
        });
    }

    private void postMessage(final Payments[] payments){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPaymentFetchSuccess(payments);
            }
        });
    }

    @Override
    public void run() {
        final PaymentsWrapper paymentsWrapper = mRepository.fetchPayments(userId, apiToken);
        if (paymentsWrapper == null) {
            notifyError("Something went wrong");
        } else if (!paymentsWrapper.status) {
            notifyError(paymentsWrapper.message);
        } else {
            postMessage(paymentsWrapper.payments);
        }
    }
}
