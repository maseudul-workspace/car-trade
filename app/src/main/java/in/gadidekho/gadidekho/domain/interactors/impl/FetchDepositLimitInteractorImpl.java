package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchDepositLimitInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.DepositLimit;
import in.gadidekho.gadidekho.domain.models.DepositLimitWrapper;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class FetchDepositLimitInteractorImpl extends AbstractInteractor implements FetchDepositLimitInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;

    public FetchDepositLimitInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDepositLimitFail(errorMsg);
            }
        });
    }

    private void postMessage(DepositLimit depositLimit){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingDepositLimitSuccess(depositLimit);
            }
        });
    }

    @Override
    public void run() {
        final DepositLimitWrapper depositLimitWrapper = mRepository.fetchDepositLimit(userId, apiToken);
        if (depositLimitWrapper == null) {
           notifyError("");
        } else if (!depositLimitWrapper.status) {
            notifyError("");
        } else {
            postMessage(depositLimitWrapper.depositLimit);
        }
    }
}
