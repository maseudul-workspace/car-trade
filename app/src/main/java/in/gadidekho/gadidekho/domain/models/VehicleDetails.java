package in.gadidekho.gadidekho.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class VehicleDetails {

    @SerializedName("time")
    @Expose
    public String time;

    @SerializedName("status")
    @Expose
    public String status;

    @SerializedName("vehicle_id")
    @Expose
    public int vehicleId;

    @SerializedName("vehicle_name")
    @Expose
    public String vehicleName;

    @SerializedName("images")
    @Expose
    public Images[] images;

    @SerializedName("regisation_no")
    @Expose
    public String registrationNo;

    @SerializedName("regisation_available")
    @Expose
    public String registrationAvailable;

    @SerializedName("mfg_month_year")
    @Expose
    public String mfg_month_year;

    @SerializedName("fuel_type")
    @Expose
    public String fuel_type;

    @SerializedName("owner_type")
    @Expose
    public String owner_type;

    @SerializedName("state")
    @Expose
    public String state;

    @SerializedName("transmission_type")
    @Expose
    public String transmission_type;

    @SerializedName("total_remaining_bids")
    @Expose
    public int total_remaining_bids;

    @SerializedName("current_bid_amount")
    @Expose
    public int current_bid_amount;

    @SerializedName("bc_mfg_month_year")
    @Expose
    public String bc_mfg_month_year;

    @SerializedName("bc_color")
    @Expose
    public String bc_color;

    @SerializedName("bc_engine_no")
    @Expose
    public String bc_engine_no;

    @SerializedName("bc_chasis_no")
    @Expose
    public String bc_chasis_no;

    @SerializedName("bc_transmission_type")
    @Expose
    public String bc_transmission_type;

    @SerializedName("bc_fuel_type")
    @Expose
    public String bc_fuel_type;

    @SerializedName("bc_owner_type")
    @Expose
    public String bc_owner_type;

    @SerializedName("bc_vehicle_type")
    @Expose
    public String bc_vehicle_type;

    @SerializedName("bc_ownership")
    @Expose
    public String bc_ownership;

    @SerializedName("rc_rc_available")
    @Expose
    public String rc_rc_available;

    @SerializedName("rc_registration_no")
    @Expose
    public String rc_registration_no;

    @SerializedName("rc_registration_date")
    @Expose
    public String rc_registration_date;

    @SerializedName("rc_reg_as")
    @Expose
    public String rc_reg_as;

    @SerializedName("tx_road_text_expiray_date")
    @Expose
    public String tx_road_text_expiray_date;

    @SerializedName("tx_permit_type")
    @Expose
    public String tx_permit_type;

    @SerializedName("tx_permit_expiray_date")
    @Expose
    public String tx_permit_expiray_date;

    @SerializedName("tx_fitness_expiray_date")
    @Expose
    public String tx_fitness_expiray_date;

    @SerializedName("tx_road_taxt_validity")
    @Expose
    public String tx_road_taxt_validity;

    @SerializedName("hi_car_under_hypothecation")
    @Expose
    public String hi_car_under_hypothecation;

    @SerializedName("hi_financer_name")
    @Expose
    public String hi_financer_name;

    @SerializedName("hi_noc_available")
    @Expose
    public String hi_noc_available;

    @SerializedName("hi_repo_date")
    @Expose
    public String hi_repo_date;

    @SerializedName("hi_loan_paid_off")
    @Expose
    public String hi_loan_paid_off;

    @SerializedName("li_zone")
    @Expose
    public String li_zone;

    @SerializedName("li_state")
    @Expose
    public String li_state;

    @SerializedName("li_city")
    @Expose
    public String li_city;

    @SerializedName("li_yard_name")
    @Expose
    public String li_yard_name;

    @SerializedName("li_yard_location")
    @Expose
    public String li_yard_location;

    @SerializedName("avi_superdari_status")
    @Expose
    public String avi_superdari_status;

    @SerializedName("avi_tax_type")
    @Expose
    public String avi_tax_type;

    @SerializedName("avi_theft_recover")
    @Expose
    public String avi_theft_recover;

    @SerializedName("avi_keys_available")
    @Expose
    public String avi_keys_available;

    @SerializedName("summary")
    @Expose
    public String summary;

    public boolean isWishListPresent = false;


}
