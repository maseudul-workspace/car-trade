package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchAuctionVehiclesInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.AuctionVehicleWrapper;
import in.gadidekho.gadidekho.repository.AppRepository;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class FetchAucionVehiclesInteractorImpl extends AbstractInteractor implements FetchAuctionVehiclesInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;
    int auctionId;

    public FetchAucionVehiclesInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken, int auctionId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
        this.auctionId = auctionId;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchVehbiclesFailed(errorMsg);
            }
        });
    }

    private void postMessage(final AuctionVehicle[] auctionVehicles){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFetchVehiclesSuccess(auctionVehicles);
            }
        });
    }

    @Override
    public void run() {
        final AuctionVehicleWrapper auctionVehicleWrapper = mRepository.fetchAuctionVehiclesWrapper(userId, apiToken, auctionId);
        if (auctionVehicleWrapper == null) {
            notifyError("Something went wrong");
        } else if (!auctionVehicleWrapper.status) {
            notifyError(auctionVehicleWrapper.message);
        } else {
            postMessage(auctionVehicleWrapper.auctionVehicles);
        }
    }
}
