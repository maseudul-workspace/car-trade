package in.gadidekho.gadidekho.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UserDetails {
    @SerializedName("userId")
    @Expose
    public int userId;

    @SerializedName("userName")
    @Expose
    public String userName;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobileNo")
    @Expose
    public String mobileNo;

    @SerializedName("address")
    @Expose
    public String address;

}
