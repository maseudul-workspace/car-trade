package in.gadidekho.gadidekho.domain.interactors;

import in.gadidekho.gadidekho.domain.models.AuctionVehicle;

public interface FetchAuctionVehiclesInteractor {
    interface Callback {
        void onFetchVehiclesSuccess(AuctionVehicle[] auctionVehicles);
        void onFetchVehbiclesFailed(String errorMsg);
    }
}
