package in.gadidekho.gadidekho.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class UpcomingAuction {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("auction_group_name")
    @Expose
    public String auctionGroupName;

    @SerializedName("auction_start_date")
    @Expose
    public String auctionStartDate;

    @SerializedName("auction_end_date")
    @Expose
    public String auctionEndDate;

    @SerializedName("total_vehicle")
    @Expose
    public int total_vehicle;

    @SerializedName("time")
    @Expose
    public String time;


}
