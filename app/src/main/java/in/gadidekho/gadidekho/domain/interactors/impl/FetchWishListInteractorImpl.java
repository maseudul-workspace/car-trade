package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchWishListInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.AuctionVehicleWrapper;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class FetchWishListInteractorImpl extends AbstractInteractor implements FetchWishListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiKey;

    public FetchWishListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiKey) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiKey = apiKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWishListFail(errorMsg);
            }
        });
    }

    private void postMessage(final AuctionVehicle[] auctionVehicles){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingWishListSuccess(auctionVehicles);
            }
        });
    }

    @Override
    public void run() {
        final AuctionVehicleWrapper auctionVehicleWrapper = mRepository.fetchWishList(userId, apiKey);
        if (auctionVehicleWrapper == null) {
            notifyError("Something went wrong");
        } else if (!auctionVehicleWrapper.status) {
            notifyError(auctionVehicleWrapper.message);
        } else {
            postMessage(auctionVehicleWrapper.auctionVehicles);
        }
    }
}
