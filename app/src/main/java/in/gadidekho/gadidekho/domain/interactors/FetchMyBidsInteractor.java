package in.gadidekho.gadidekho.domain.interactors;

import in.gadidekho.gadidekho.domain.models.AuctionVehicle;

public interface FetchMyBidsInteractor {
    interface Callback {
        void onGettingMyBidsSuccess(AuctionVehicle[] auctionVehicles);
        void onGettingMyBidsFail(String errorMsg);
    }
}
