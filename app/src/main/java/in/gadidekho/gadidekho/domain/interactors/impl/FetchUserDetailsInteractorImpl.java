package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchUserDetailsInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.UserDetails;
import in.gadidekho.gadidekho.domain.models.UserDetailsWrapper;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class FetchUserDetailsInteractorImpl extends AbstractInteractor implements FetchUserDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiToken;

    public FetchUserDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(final UserDetails userDetails){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingUserDetailsSuccess(userDetails);
            }
        });
    }

    @Override
    public void run() {
        UserDetailsWrapper userDetailsWrapper = mRepository.fetchUserDetails(userId, apiToken);
        if (userDetailsWrapper == null) {
            notifyError("Something went wrong");
        } else if (!userDetailsWrapper.status) {
            notifyError(userDetailsWrapper.message);
        } else {
            postMessage(userDetailsWrapper.userDetails);
        }
    }
}
