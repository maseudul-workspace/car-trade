package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchMyWinsInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.AuctionVehicle;
import in.gadidekho.gadidekho.domain.models.AuctionVehicleWrapper;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class FetchMyWinsInteractorImpl extends AbstractInteractor implements FetchMyWinsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int userId;
    String apiKey;

    public FetchMyWinsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int userId, String apiKey) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.userId = userId;
        this.apiKey = apiKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMyWinsFail(errorMsg);
            }
        });
    }

    private void postMessage(final AuctionVehicle[] auctionVehicles){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingMyWinsSuccess(auctionVehicles);
            }
        });
    }

    @Override
    public void run() {
        final AuctionVehicleWrapper auctionVehicleWrapper = mRepository.fetchMyWins(userId, apiKey);
        if (auctionVehicleWrapper == null) {
            notifyError("Something went wrong");
        } else if (!auctionVehicleWrapper.status) {
            notifyError(auctionVehicleWrapper.message);
        } else {
            postMessage(auctionVehicleWrapper.auctionVehicles);
        }
    }
}
