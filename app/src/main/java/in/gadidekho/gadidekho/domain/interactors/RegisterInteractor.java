package in.gadidekho.gadidekho.domain.interactors;

public interface RegisterInteractor {
    interface Callback{
        void onRegisterSuccess();
        void onRegsiterFail();
    }
}
