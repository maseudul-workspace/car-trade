package in.gadidekho.gadidekho.domain.interactors;

public interface SubmitBidInteractor {
    interface Callback {
        void onBiddingSuccess();
        void onBiddingFail();
    }
}
