package in.gadidekho.gadidekho.domain.interactors.impl;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.FetchNotificationsInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.Notification;
import in.gadidekho.gadidekho.domain.models.NotificationsWrapper;
import in.gadidekho.gadidekho.domain.models.UserInfo;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class FetchNotificationsInteractorImpl extends AbstractInteractor implements FetchNotificationsInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    int userId;
    String apiToken;

    public FetchNotificationsInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, int userId, String apiToken) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.userId = userId;
        this.apiToken = apiToken;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingNotificationFail(errorMsg);
            }
        });
    }

    private void postMessage(final Notification[] notifications){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingNotificationsSuccess(notifications);
            }
        });
    }

    @Override
    public void run() {
        final NotificationsWrapper notificationsWrapper = mRepository.getNotifications(userId, apiToken);
        if (notificationsWrapper == null) {
            notifyError("Something went wrong");
        } else if (!notificationsWrapper.status) {
            notifyError(notificationsWrapper.message);
        } else {
            postMessage(notificationsWrapper.notifications);
        }
    }
}
