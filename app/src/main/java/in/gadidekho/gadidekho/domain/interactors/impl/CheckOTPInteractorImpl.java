package in.gadidekho.gadidekho.domain.interactors.impl;

import android.telecom.Call;

import in.gadidekho.gadidekho.domain.executors.Executor;
import in.gadidekho.gadidekho.domain.executors.MainThread;
import in.gadidekho.gadidekho.domain.interactors.CheckOTPInteractor;
import in.gadidekho.gadidekho.domain.interactors.base.AbstractInteractor;
import in.gadidekho.gadidekho.domain.models.SuccessResponse;
import in.gadidekho.gadidekho.repository.AppRepositoryImpl;

public class CheckOTPInteractorImpl extends AbstractInteractor implements CheckOTPInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String mobile;

    public CheckOTPInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String mobile) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.mobile = mobile;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckOTPFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onCheckOTPSucces();
            }
        });
    }

    @Override
    public void run() {
        final SuccessResponse successResponse = mRepository.checkOTP(mobile);
        if (successResponse == null) {
            notifyError("Something went wrong");
        } else if (!successResponse.status) {
            notifyError(successResponse.message);
        } else {
            postMessage();
        }
    }
}
